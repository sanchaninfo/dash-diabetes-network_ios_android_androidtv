    var ssi;
    var s = 0;

    function loadConfig() {
        $.ajax({
            url: ServerUrl+"gets3FilesMobile",
            type: "GET",
            data: {},
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: function(data) {
              
                var bodyArray = (data);
                if ($("#copyrts").length) {
                    $("#copyrts").html(bodyArray.copyRight);
                };
                if ($("#trms").length) {
                    $("#trms").html('<a href="#" onClick=javascript:loadPage("' + bodyArray.termsofUse + '")>Terms of Use</a>');
                };
                if ($("#priv").length) {
                    $("#priv").html('<a href="#" onClick=javascript:loadPage("' + bodyArray.privacyPolicy + '")>Privacy</a>');
                };
                if ($("#supp").length) {
                    $("#supp").html('<a href="#" onClick=javascript:loadPage("' + bodyArray.support + '")>Support</a>');
                };
                $("#sourceURL").val(bodyArray.sourceurl);

                if ($(window).width() < 400) {
                    ssi = bodyArray.mobileSlideShowProtrait;
                } else {
                    ssi = bodyArray.mobileSlideShowLanscape;
                }

                if ($("#bxslider").length) {
                    slideShow();
                }
               
             if(bodyArray.mobile_damedash[0].menu_terms== "true")
               {
               
               $('#Terms').show();
               }
               else{
               
               $('#Terms').hide();
               }
            if(bodyArray.mobile_damedash[0].menu_privacy== "true")
               {
               $('#privacy').show();
               }
               else{
               $('#privacy').hide();
               }
            if(bodyArray.mobile_damedash[0].menu_support== "true")
               {
               $('#support').show();
               }
               else{
               $('#support').hide();
               }
            if(bodyArray.mobile_damedash[0].footer_movies== "true")
               {
               $('#movies').show();
               }
               else{
               $('#movies').hide();
               }
            if(bodyArray.mobile_damedash[0].footer_photos== "true")
               {
               $('#photos').show();
               }
               else{
               $('#photos').hide();
               }
            if(bodyArray.mobile_damedash[0].footer_shows== "true")
               {
               $('#shows').show();
               }
               else{
               $('#shows').hide();
               }
            if(bodyArray.mobile_damedash[0].footer_music== "true")
               {
               $('#music').show();
               }
               else{
               $('#music').hide();
               }
            if(bodyArray.mobile_damedash[0].footer_terms== "true")
               {
               $('#footer_terms').show();
               }
               else{
               $('#footer_terms').hide();
               }
            if(bodyArray.mobile_damedash[0].footer_privacy== "true")
               {
               $('#footer_privacy').show();
               }
               else{
               $('#footer_privacy').hide();
               }
            if(bodyArray.mobile_damedash[0].footer_support== "true")
               {
               $('#footer_support').show();
               }
               else{
               $('#footer_support').hide();
               }
            if(bodyArray.mobile_damedash[0].footer_poweredby== "true")
               {
               $('#poweredby').show();
               }
               else{
               $('#poweredby').hide();
               }
            if(bodyArray.mobile_damedash[0].menu_store== "true")
                  {
                  $('#store').show();
                  }
                  else{
                  $('#store').hide();
                  }
               if(bodyArray.mobile_damedash[0].footer_movies== "false" && bodyArray.mobile_damedash[0].footer_photos== "false" && bodyArray.mobile_damedash[0].footer_shows== "false" && bodyArray.mobile_damedash[0].footer_music== "false")
               {
               $('#catdiv').addClass("n_ctgy");
               $('#foot').addClass("n_fotter");
               }
               else{
               $('#catdiv').removeClass("n_ctgy");
               $('#foot').removeClass("n_fotter");
               }
               
               if(bodyArray.mobile_damedash[0].footer_terms== "false" && bodyArray.mobile_damedash[0].footer_privacy== "false" && bodyArray.mobile_damedash[0].footer_support== "false" && bodyArray.mobile_damedash[0].footer_poweredby== "false")
               {
               $('#foot').hide();
               }
               else{
               $('#foot').show();
               }
//               console.log(bodyArray.mobile_damedash);
//               console.log(bodyArray.mobile_damedash[0].menu_terms);
               
              
               $("#fb_link").attr("href",bodyArray.DashDiabetesNetworkSocialLinks[0].url)
               $("#twitter_link").attr("href",bodyArray.DashDiabetesNetworkSocialLinks[6].url)
               $("#youtube_link").attr("href",bodyArray.DashDiabetesNetworkSocialLinks[8].url)
               $("#instagram_link").attr("href",bodyArray.DashDiabetesNetworkSocialLinks[7].url)
            
               },
            error: function(jqXHR, textStatus, errorThrown) {
                //alert("Retrive config error.");
            },
            complete: function() {

            }
        });

    }

    function slideShow() {
        var imgLen = ssi.length;
        var bxslider = '';
        $.each(ssi, function(key, value) {
            bxslider = bxslider + '<li><img src="' + $("#sourceURL").val() + '/' + value + '" title="" /></li>';
            if (key + 1 == imgLen) {
                $("#bxslider").html(bxslider);
                $('.bxslider').bxSlider({
                    auto: true,
                    mode: 'horizontal',
                    controls: false,
                    pager: false,
                    touchEnabled: true,
                    speed: 300,
                    easing: 'ease-out'
                });
            }
        });
    }
