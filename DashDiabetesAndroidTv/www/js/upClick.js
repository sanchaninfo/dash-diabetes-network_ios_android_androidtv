/*
     Module Name :
     File Name   :      upClick.js
     Project     :      dev.damedashstudios.com
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      0.0.12 
     Created on  :      9th october 
     Last modified on:  30th November
     Description :      upclick functionality for fire tv remote control
     Organisation:      Peafowl inc.
*/
function upClickFunction() {
    isDescriptionDivVisible = $('#descriptionDiv').is(':visible');
    isThumbsListVisible = $('#thumbsList').is(':visible');
    var isEpisode3Visible = $('#episode_action3').is(':visible');
    var isButtonShow = $('#buttonShow').is(':visible');
    var isEpisodeThumbsVisible = $('#episodeButtons').is(':visible');
    var isEpisodeResumeVisible = $('#episode_actionResume').is(':visible');
    var isdownmenuDiv = $('#menuDiv').is(':visible');
    var catVisible = $('#Catecomplete_' + countDownClickVar).is(':visible');
    var searchDivVisible = $('#divSearch').is(':visible');
    var searchBorder = $('#divimageBorderSearch').is(':visible');
     var settingsDiv = $('#divSettingsMain').is(':visible');
    if (isEpisodeThumbsVisible) {
        //$.fn.als("prev",cnt);
        if ($("#episodeDescription").hasClass("episodeInactive")) {
            episodeAssetDownClick--;
            var assetData = $("#Episode_" + episodeDiv + "_" + episodeAssetDownClick).attr("title");
            if (assetData != null) {
                episodetopMov = episodetopMov - topmovdownclickEpisode;
                $("#episodeSeason_" + episodeDiv).animate({
                    marginTop: -episodetopMov + "px"
                }, 500);
            } else {
                episodeAssetDownClick++;
            }
        } else {
            if (episodeDiv == 0) {
                episodeDiv = 0;
            } else {

                $("#season_" + episodeDiv).removeClass('catActive');
                //$("#Catecomplete_"+countDownClickVar).fadeOut();
                $("#episodeSeason_" + episodeDiv).hide();
                episodeDiv--;
                $("#season_" + episodeDiv).addClass('catActive');
                $("#episodeSeason_" + episodeDiv).show();

            }
        }
    } else if (isDescriptionDivVisible && isButtonShow) {
        descriptionUpClick(isEpisode3Visible, isEpisodeResumeVisible, resumeEpisodeStat)
    } else if (isThumbsListVisible) {
        $('#rating').html("");
        count--;
        if (countDownClickVarLanding == 0) {
            countDownClickVarLanding = 0;
			slickVal = 0;
        } else {
            countDownClickVarLanding--;
			slickVal--;
            $("#cate_" + countDownClickVarLanding).animate({
                marginTop: '0px'
            }, 500);
            var assetValues = $('#assetid' + countDownClickVarLanding).val();
            assetDetailsSplit(assetValues);
        }
    } else if (isdownmenuDiv) {
        if (countDownClickVar == 0) {
            countDownClickVar = 0;
        } else {

            $("#Catedata_" + countDownClickVar).removeClass('cate_buttonactive');
            //$("#Catecomplete_"+countDownClickVar).fadeOut();
            $("#Catecomplete_" + countDownClickVar).hide();
            countDownClickVar--;
            var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            assetDetailsSplitCat(assetData);
            $("#Catedata_" + countDownClickVar).addClass('cate_buttonactive');
            $("#Catecomplete_" + countDownClickVar).show();

        }
    } else if (catVisible) {
        upClickCat++;
        categoryDownClick = categoryDownClick - 5;
        var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
        if (assetData != null) {
            topMov = topMov - topmovdownclick;
            assetDetailsSplitCat(assetData);
            $("#Catecomplete_" + countDownClickVar).animate({
                marginTop: -topMov + "px"
            }, 500);
        } else {
            categoryDownClick = categoryDownClick + 5;
        }
    } else if (searchDivVisible) {
        if (searchBorder) {
            searchCountAsset = searchCountAsset - 3;
            var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
            if (assetValSearch != null) {
                topMovSearch = topMovSearch - topMovSearchDim;
                $("#divsearchAssetCont").animate({
                    marginTop: -topMovSearch + "px"
                }, 500);
            } else {
                searchCountAsset = searchCountAsset + 3;
            }
        } else {
            if (countDownKeyPad > 0) {

                countDownKeyPad--;
                if (keyPadValue > 3) {
                    if (countDownKeyPad == 0) {

                        keyPadValue = 2;
                        $("#" + keyPadCount + "g").removeClass("searchCont_active");
                        $("#" + keyPadCount + "g").css({
                            color: 'white'
                        });
                        keyPadCount = 1;
                        $("#" + keyPadCount + "g").addClass("searchCont_active");
                        $("#" + keyPadCount + "g").css({
                            color: 'black'
                        });
                    } else {

                        leftClickSearchFun = leftClickSearchFun - 6;

                        keyPadValue = keyPadValue - 6;

                        $("#" + keyPadCount + "g").removeClass("searchCont_active");
                        $("#" + keyPadCount + "g").css({
                            color: 'white'
                        });
                        keyPadCount = keyPadCount - 6;
                        $("#" + keyPadCount + "g").addClass("searchCont_active");
                        $("#" + keyPadCount + "g").css({
                            color: 'black'
                        });
                    }

                }

            }
            else{
                $("#" + keyPadCount + "g").removeClass("searchCont_active");
                        $("#" + keyPadCount + "g").css({
                            color: 'white'
                        });
                $("#divKeyBoardBackBtn").addClass("searchCont_active");
                $("#divKeyBoardBackBtn").css({
                            color: 'black'
                        });
            }
        }

    }else if(settingsDiv){
        if(settingsClick > 0){
                $("#divSettingsMainExitBtn").removeClass("src_nav_settings_active");
                settingsClick --;
                $("#divSettingsMainBackBtn").addClass("src_nav_settings_active"); 
                $("#gbBackBrowse").addClass("gb_act");              
        }
    }
}