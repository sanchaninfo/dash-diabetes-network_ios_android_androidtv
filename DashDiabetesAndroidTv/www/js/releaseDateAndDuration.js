/*
     Module Name :
     File Name   :      releaseDateAndDuration.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      0.0.12 
     Created on  :      8 august 12:26 pm
     Last modified on:  30 November
     Description :      converting release date and time into hours and minutes.
     Organisation:      Peafowl inc.
*/

function releaseDateAndDurationInLanding(duration,listYear)
{
               var ms=parseInt(duration); 
               var seconds = (ms/1000);
               var minutes = parseInt(seconds/60, 10);
               seconds = seconds%60;
               var hours = parseInt(minutes/60, 10);
               minutes = minutes%60; 
               if(hours==0)
               {
                 $('#movieDuration').html(minutes+"m");
                 $('#movieRelease').html(yearOfRelease(listYear)); 
               }
               else
               {
                 $('#movieDuration').html(hours+"h "+minutes+"m");
                 $('#movieRelease').html(yearOfRelease(listYear)); 
               }    
}

function releaseDateAndDurationInCat(duration,listYear)
{
               var ms=parseInt(duration); 
               var seconds = (ms/1000);
               var minutes = parseInt(seconds/60, 10);
               seconds = seconds%60;
               var hours = parseInt(minutes/60, 10);
               minutes = minutes%60; 
               if(hours==0)
               {
                 $('#movieDurationCat').html(minutes+"m");
                 $('#movieReleaseCat').html(yearOfRelease(listYear)); 
               }
               else
               {
                 $('#movieDurationCat').html(hours+"h "+minutes+"m");
                 $('#movieReleaseCat').html(yearOfRelease(listYear)); 
               }    
}