/*
     Module Name :
     File Name   :      enterFunction.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 
     Created on  :      8 august 12:26 pm
     Last modified on:  1st November
     Description :      functionality for enter click
     Organisation:      Peafowl inc.
*/
function enterClickFunction() {

    isDescriptionDivVisible = $('#descriptionDivDescribe').is(':visible');
    isThumbsListVisible = $('#thumbsList').is(':visible');
    var isEpisodeThumbsVisible = $('#episodeButtons').is(':visible');
    var isEpisode3Visible = $('#episode_action3').is(':visible');
    var isEpisodeResumeVisible = $('#episode_actionResume').is(':visible');
    var ismenuDiv = $('#menuDiv').is(':visible');
    var catVisible = $('#Catecomplete_' + countDownClickVar).is(':visible');
    var catSelectVisible = $('#categorySelectMain').is(':visible');
    var videoDivVisible = $('#video_player').is(':visible');
    var searchDivVisible = $('#divSearch').is(':visible');
    var searchBorder = $('#divimageBorderSearch').is(':visible');
    var settingsDiv = $('#divSettingsMain').is(':visible');
    var Store = window.localStorage;
    if (isEpisodeThumbsVisible) {

        var episodeValue = $("#Episode_" + episodeDiv + "_" + episodeAssetDownClick).attr("title");
        var epiAssetVal = [];
        epiAssetVal = episodeValue.split('|');
        videoPlayerInEpisodes = 1;
        playVidInEpisodes = 1;
        episodeVideoPlay(epiAssetVal[0], epiAssetVal[1]);
    }else if(slideShower == 1){
  //alert(assetSlideId); 
 
  setTimeout(function() {  
                  //alert(assetSlideId); 
                getassetLikes(assetSlideId,"photo")
    }, 2000);
}
     else if (catVisible == true && ismenuDiv == false) {

        if (descriptionFlag == 0 || descriptionFlag == 1) {
            flagMainDivVisible = 1;
            flagAddListBack = 0;
            var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            $("#catVisible").fadeOut();
            $("#descTxtCat").fadeOut();
            $("#Catecomplete_" + countDownClickVar).fadeOut();
            $("#image1").fadeOut();
            $(".preloaderContDescribe").fadeIn();
            var splitAssetDetails = [];
            splitAssetDetails = assetDataValues.split('|');

              if(Store.getItem(userId) == null ){
                
                        createDataBase();
            }
            else{
                 displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
           }
            //displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
        }
    } else if (ismenuDiv) {
        $("#menuDiv").fadeOut();
        $("#CategroyDataDiv").css({
            marginRight: "-1%",
            width: "98%"
        });
        $("#image1").fadeIn();
        $(".categoryData .thumbsList li").css("width", "18%");
        $("#descTxtCat").fadeIn();
        assetDetailsSplitCat(assetDataValues);
    } else if (isThumbsListVisible) {
        if (descriptionFlag == 0 || descriptionFlag == 1) {
        /*     if(countDownClickVarLanding == cnt-1){
               
             $("#descTxt").fadeOut();
                $("#desc_cont_landingPage").fadeOut();
               // $("#movieThumbnails").fadeOut();
                $('#movieThumbnails').css({ opacity: 1 });
                $("#divID").fadeOut();
                $("#landing_BackgroundImage").fadeOut();
                $("#landingPage").hide();
               // $(".gradient_class").hide();
                //$(".landingPage_blackscreen").hide();
                $("#MainDiv").css({ opacity: 0 });
                $('#slideShowImg').css({ opacity: 1 });
                slideShower = 1;
                mainDivFlag = 0;
        }*/
       /* else if(countDownClickVarLanding == cnt-2){
            window.location.href = 'indexDevStore.html';
        }*/
       // else{
           
                var subscriptionValue = window.localStorage;
                flagMainDivVisible = 0;
                flagAddListBack = 0;
                //var networkCheck = checkNetworkConnectivity();
                var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails = [];
                splitAssetDetails = assetValueTest.split('|');
            //if(networkCheck == 1){
                  if(Store.getItem(userId) == null ){
                        createDataBase();
            }
            else{
                 displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
           }
                // displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
                $("#descTxt").fadeOut();
                $("#desc_cont_landingPage").fadeOut();
                $("#movieThumbnails").fadeOut();
                $("#divID").fadeOut();
                $(".preloaderContDescribe").fadeIn();
           // }
       // }
        }
    } else if (catSelectVisible) {
        if (catSelectCount == 1) { //selection of search page
            $("#landingPage").fadeOut();
            $("#categorySelectMain").fadeOut();
            $("#divSearch").fadeIn();
            $("#1g").css({
                color: 'black'
            });;
        } else if (catSelectCount == 2) { //selection of category page
            $("#landingPage").fadeOut();
            $("#categorySelectMain").fadeOut();
            $("#catBackground").fadeIn();
            $("#CategoryPage").fadeIn();
            $("#menuDiv").fadeIn();
            $("#Catedata_" + countDownClickVar).fadeIn();
            $("#Catecomplete_" + countDownClickVar).fadeIn();
            $("#Catedata_" + countDownClickVar).addClass("cate_buttonactive");
            var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            assetDetailsSplitCat(assetData);
        } else if (catSelectCount == 3) { //selection of settings page
              var storage = window.localStorage;
             if(Store.getItem(userId) == null ){
                        createDataBase();
            }
            else{

                  var dateFormat = new Date(storage.getItem(subscription_end));
            var date = dateFormat.getDate();
            var year = dateFormat.getFullYear();
            var month = dateFormat.getMonth()+1;
            $("#landingPage").fadeOut();
            $("#categorySelectMain").fadeOut();
            $("#divSettingsMain").fadeIn();
            $(".videoContEmail").html(storage.getItem(email));
            var strPlan = storage.getItem(subscription_type);
            strPlan = strPlan.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            $(".videoContPlan").html(strPlan);
            if(storage.getItem(subscription_end) == ""){
                $(".videoContExpiry").html("");
            }
            else{
                $(".videoContExpiry").html(month + "/" + date + "/" + year);
            }
            
            }
        } else if (catSelectCount == 4) { //selection for exiting damedash studios
            navigator.app.exitApp();
        }
    }
      else if(episodeClick == 5){

        if($("#episode_action"+(episodeClick-1)).hasClass("star_active1")){

               var activeClassCountAvg =0;
               var storage = window.localStorage;
                flagAddList = 1;
                  $.ajax({
                    type: "POST",
                   // url: URL_LINK + "/userAssetRating",


                    url: ServerLamdaUrl + "userassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName+ "&user_rating=" + startRateCount+ "&previousrate=" + userratingsum,
                    /*data: JSON.stringify({
                        "userAssetRating": {
                            "assetId": globalassetId,
                            "userId": storingIn,
                            "rating":startRateCount,
                            "previousRate":userratingsum
                        }
                    }),*/
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {

                        $.ajax({
                    type: "POST",
                    //url: URL_LINK + "/getAssetRating",
                    url: ServerLamdaUrl + "getassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (dataRate) {
                        var data = dataRate.result;
                       // addOrRemoveAssetToList(data.assetId, data, data.userData[0].myList, flagNextAsset);
                       userratingsum = data[0].userassetrating;
                       startRateCount = data[0].userassetrating;
                       avgAssetRating = data[0].averagerating;
                       
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                       
                          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                         
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                    userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);

                    }
                });
                    }
                });
            }
    }
    if (episodeClick == 3) {

        if (isEpisode3Visible) {
            //alert('test')
         if($("#episode_action"+episodeClick).hasClass("star_active1")){
               var activeClassCountAvg =0;
               var storage = window.localStorage;
                flagAddList = 1;
                  $.ajax({
                    type: "POST",
                   // url: URL_LINK + "/userAssetRating",


                    url: ServerLamdaUrl + "userassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName+ "&user_rating=" + startRateCount+ "&previousrate=" + userratingsum,
                    /*data: JSON.stringify({
                        "userAssetRating": {
                            "assetId": globalassetId,
                            "userId": storingIn,
                            "rating":startRateCount,
                            "previousRate":userratingsum
                        }
                    }),*/
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {

                        $.ajax({
                    type: "POST",
                    //url: URL_LINK + "/getAssetRating",
                    url: ServerLamdaUrl + "getassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (dataRate) {
                        var data = dataRate.result;
                       // addOrRemoveAssetToList(data.assetId, data, data.userData[0].myList, flagNextAsset);
                       userratingsum = data[0].userassetrating;
                       startRateCount = data[0].userassetrating;
                       avgAssetRating = data[0].averagerating;
                       
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                       
                          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                         
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                    userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);

                    }
                });
                    }
                });
            }
            else{
               
                   if (resumeEpisodeStat == 1) {
                if (divVisibleLanding == 1) {
                    var assetValueTest1 = $('#assetid' + countDownClickVarLanding).val();
                    var splitAssetDetails1 = [];
                    splitAssetDetails1 = assetValueTest1.split('|');
                    flagButtonMovement = 1;
                    addOrRemoveFromMyList(splitAssetDetails1[0],0);
                } else if (divVisibleLanding == 0) {
                    if (flagSearchDescribe == 1) {
                        var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        var splitAssetDetailsSearch = [];
                        splitAssetDetailsSearch = assetValueSearch.split('|');
                        flagButtonMovement = 1;
                        addOrRemoveFromMyList(splitAssetDetailsSearch[0],0);
                    } else {
                        var assetValueTest1 = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                        var splitAssetDetails1 = [];
                        splitAssetDetails1 = assetValueTest1.split('|');
                        flagButtonMovement = 1;
                        addOrRemoveFromMyList(splitAssetDetails1[0],0);
                    }

                }
            } else {
                if($("#episode_action"+episodeClick).hasClass("episodes_landingPage")){
                   
                     landingCarouselFlag = 0;
                episodeCarouselFlag = 1;
                $("#descriptionDiv").fadeOut();
                if (pageVisible == 0) {

                    var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                    var splitAssetDetails = [];
                    splitAssetDetails = assetValueTest.split('|');
                    //$('#department').load(getEpisodedetails(splitAssetDetails[0]));
                    $("#landingPage").fadeOut();
                    getEpisodedetails(splitAssetDetails[0]);
                } else {
                    if (flagSearchDescribe == 1) {
                        var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        var splitAssetDetailsSearch = [];
                        splitAssetDetailsSearch = assetValueSearch.split('|');
                        $("#landingPageSearch").fadeOut();
                        getEpisodedetails(splitAssetDetailsSearch[0]);
                    } else {
                        var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                        var splitAssetDetails = [];
                        splitAssetDetails = assetDataValues.split('|');
                        $("#catBackground").fadeOut();
                        getEpisodedetails(splitAssetDetails[0]);
                    }
                }
                }
                else{
                   
                     var assetValueTest1 = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails1 = [];
                splitAssetDetails1 = assetValueTest1.split('|');
               
                //addOrRemoveFromMyList(splitAssetDetails1[0],0);

                   if(Store.getItem(userId) == null ){
                        createDataBase();
                       }
                       else{
                         addOrRemoveFromMyList(splitAssetDetails1[0],0);
                       }
                }
                
            }
            }
        } else if (isEpisodeResumeVisible == true) {

            if (divVisibleLanding == 1) {
                var assetValueTest1 = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails1 = [];
                splitAssetDetails1 = assetValueTest1.split('|');
                flagButtonMovement = 1;
                addOrRemoveFromMyList(splitAssetDetails1[0]);
            } else if (divVisibleLanding == 0) {
                if (flagSearchDescribe == 1) {
                    var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                    var splitAssetDetailsSearch = [];
                    splitAssetDetailsSearch = assetValueSearch.split('|');
                    flagButtonMovement = 1;
                    addOrRemoveFromMyList(splitAssetDetailsSearch[0]);
                } else {
                    var assetValueTest1 = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                    var splitAssetDetails1 = [];
                    splitAssetDetails1 = assetValueTest1.split('|');
                    flagButtonMovement = 1;
                    addOrRemoveFromMyList(splitAssetDetails1[0]);
                }

            }
        }
        else if($("#likeSelect").hasClass('likelist_active')){
        
            if(flagLike == 0){
            
                $("#dislikeCnt").html(likesCount);
                $("#dislikeCntDisp").html(likesCount);
                $("#dislikeCntCat").html(likesCount);
                $("#likeSelect").html("Dislike");
                //$("#likeSelect").removeClass("likelist_active");
                 //$("#likeSelect").addClass("dislikelist_active");
                // $('.likelist_active').css('background-image',"");
               // $('.likelist_active').css('background-image','url("../images/dislike_gray.png")');
                likesCount++;
                $("#likeCnt").html(likesCount);
                $("#likeCntDisp").html(likesCount);
                $("#likeCntCat").html(likesCount);

                flagLike = 1;
              
                getassetLikes(videoAssetId,"video");
            }
            else{
               
                $("#dislikeCnt").html(likesCount);
                $("#dislikeCntDisp").html(likesCount);
                $("#dislikeCntCat").html(likesCount);
                $("#likeSelect").html("Like");
               // $("#likeSelect").addClass("likelist_active")
               //  $("#likeSelect").removeClass("dislikelist_active")
              //  $('.likelist_active').css('background-image',"");
               // $('.likelist_active').css('background-image','url("../images/like_gray.png")');
                 likesCount--;
                $("#likeCnt").html(likesCount);
                $("#likeCntDisp").html(likesCount);
                $("#likeCntCat").html(likesCount);
                flagLike = 0;
                getassetLikes(videoAssetId,"video");
            }
         }
    } else if (episodeClick == 4) {
      
         if($("#episode_action"+episodeClick).hasClass("star_active1")){
           
                var storage = window.localStorage;
                var activeClassCountAvg =0;
                flagAddList = 1;
                  $.ajax({
                    type: "POST",
                     url: ServerLamdaUrl + "userassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName+ "&user_rating=" + startRateCount+ "&previousrate=" + userratingsum,
                    /*data: JSON.stringify({
                        "userAssetRating": {
                            "assetId": globalassetId,
                            "userId": storingIn,
                            "rating":startRateCount,
                            "previousRate":userratingsum
                        }
                    }),*/
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                       
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                          $.ajax({
                    type: "POST",
                    //url: URL_LINK + "/getAssetRating",
                    url: ServerLamdaUrl + "getassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (dataRate) {
                        var data = dataRate.result;
                       // addOrRemoveAssetToList(data.assetId, data, data.userData[0].myList, flagNextAsset);
                       userratingsum = data[0].userassetrating;
                       startRateCount = data[0].userassetrating;
                       avgAssetRating = data[0].averagerating;
                       
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                       
                          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                         
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                    userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);

                    }
                });

                    }
                });
            }
            else{
               
                 if (isEpisode3Visible) {
                      if($("#episode_action"+(episodeClick-1)).hasClass("star_active1")){
                        var storage = window.localStorage;
                        var activeClassCountAvg =0;
                        flagAddList = 1;
                         $.ajax({
                    type: "POST",
                    url: ServerLamdaUrl + "userassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName+ "&user_rating=" + startRateCount+ "&previousrate=" + userratingsum,
                    /*data: JSON.stringify({
                        "userAssetRating": {
                            "assetId": globalassetId,
                            "userId": storingIn,
                            "rating":startRateCount,
                            "previousRate":userratingsum
                        }
                    }),*/
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                           $.ajax({
                    type: "POST",
                    //url: URL_LINK + "/getAssetRating",
                    url: ServerLamdaUrl + "getassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (dataRate) {
                        var data = dataRate.result;
                       // addOrRemoveAssetToList(data.assetId, data, data.userData[0].myList, flagNextAsset);
                       userratingsum = data[0].userassetrating;
                       startRateCount = data[0].userassetrating;
                       avgAssetRating = data[0].averagerating;
                       
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                       
                          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                         
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                    userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);

                    }
                });
                    }
                });
                    }
                    else{
                         landingCarouselFlag = 0;
            episodeCarouselFlag = 1;
            $("#descriptionDiv").fadeOut();
            $("#episodeDescription").fadeIn();
            if (pageVisible == 0) {
                var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails = [];
                $("#landingPage").fadeOut();
                splitAssetDetails = assetValueTest.split('|');
                getEpisodedetails(splitAssetDetails[0]);
            } else {
                if (flagSearchDescribe == 1) {
                    var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                    var splitAssetDetailsSearch = [];
                    splitAssetDetailsSearch = assetValueSearch.split('|');
                    $("#landingPageSearch").fadeOut();
                    getEpisodedetails(splitAssetDetailsSearch[0]);
                } else {
                    var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                    var splitAssetDetails = [];
                    splitAssetDetails = assetDataValues.split('|');
                    $("#catBackground").fadeOut();
                    getEpisodedetails(splitAssetDetails[0]);
                }
            }
                    }
           
        }
        else if($("#likeSelect").hasClass('likelist_active')){
          
            if(flagLike == 0){
               
                $("#dislikeCnt").html(likesCount);
                $("#dislikeCntDisp").html(likesCount);
                $("#dislikeCntCat").html(likesCount);
                 $("#likeSelect").html("Dislike");
                // $("#likeSelect").removeClass("likelist_active")
                // $("#likeSelect").addClass("dislikelist_active")
                 //$('.likelist_active').css('background-image',"");
                 // $('.likelist_active').css('background-image','url("../images/dislike_gray.png")');
                likesCount++;
                $("#likeCnt").html(likesCount);
                $("#likeCntDisp").html(likesCount);
                $("#likeCntCat").html(likesCount);
                flagLike = 1;
                getassetLikes(videoAssetId,"video");
            }
            else{
             
                $("#dislikeCnt").html(likesCount);
                $("#dislikeCntDisp").html(likesCount);
                $("#dislikeCntCat").html(likesCount);
                $("#likeSelect").html("Like");
               // $("#likeSelect").addClass("likelist_active")
               //  $("#likeSelect").removeClass("dislikelist_active")
                //$('.likelist_active').css('background-image',"");
                //$('.likelist_active').css('background-image','url("../images/like_gray.png")');
                 likesCount--;
                $("#likeCnt").html(likesCount);
                $("#likeCntDisp").html(likesCount);
                $("#likeCntCat").html(likesCount);
                flagLike = 0;
                getassetLikes(videoAssetId,"video");
            }
         }
            }
       
    } else if (isDescriptionDivVisible) {

        if (episodeClick == 1) {

            if (divVisibleLanding == 1) {
                if (isEpisodeResumeVisible || resumeEpisodeStat == 1) {

                    var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                    var splitAssetDetails = [];
                    splitAssetDetails = assetValueTest.split('|');
                    resumeButtonVisible(splitAssetDetails[0])
                } else {
                    var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                    var splitAssetDetails = [];
                    splitAssetDetails = assetValueTest.split('|');
                    updateSeekTimeAndPlay(splitAssetDetails[0])
                }
            } else if (divVisibleLanding == 0) {
                if (isEpisodeResumeVisible) {
                    if (flagSearchDescribe == 1) {
                        var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        var splitAssetDetailsSearch = [];
                        splitAssetDetailsSearch = assetValueSearch.split('|');
                        resumeButtonVisible(splitAssetDetailsSearch[0]);
                    } else {
                        var assetValueTest = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                        var splitAssetDetails = [];
                        splitAssetDetails = assetValueTest.split('|');
                        resumeButtonVisible(splitAssetDetails[0])
                    }
                } else {
                    if (flagSearchDescribe == 1) {
                        var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        var splitAssetDetailsSearch = [];
                        splitAssetDetailsSearch = assetValueSearch.split('|');
                        updateSeekTimeAndPlay(splitAssetDetailsSearch[0]);
                    } else {
                        var assetValueTest = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                        var splitAssetDetails = [];
                        splitAssetDetails = assetValueTest.split('|');
                        updateSeekTimeAndPlay(splitAssetDetails[0])
                    }
                }
            }
        } else if ((episodeClick == 2 || isEpisode3Visible) && isEpisodeResumeVisible == false) {
            if (divVisibleLanding == 1) {

                var assetValueTest1 = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails1 = [];
                splitAssetDetails1 = assetValueTest1.split('|');
                flagButtonMovement = 1;
                addOrRemoveFromMyList(splitAssetDetails1[0]);

            } else if (divVisibleLanding == 0) {
                if (flagSearchDescribe == 1) {
                    var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                    var splitAssetDetailsSearch = [];
                    splitAssetDetailsSearch = assetValueSearch.split('|');
                    flagButtonMovement = 1;
                    addOrRemoveFromMyList(splitAssetDetailsSearch[0]);
                } else {
                    var assetValueTest1 = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                    var splitAssetDetails1 = [];
                    splitAssetDetails1 = assetValueTest1.split('|');
                    flagButtonMovement = 1;
                    addOrRemoveFromMyList(splitAssetDetails1[0]);
                }

            }

        } else if (episodeClick == 2 && isEpisodeResumeVisible == true) {

            if (divVisibleLanding == 1) {
                var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails = [];
                splitAssetDetails = assetValueTest.split('|');
                updateSeekTimeAndPlay(splitAssetDetails[0]);
            } else if (divVisibleLanding == 0) {
                if (flagSearchDescribe == 1) {
                    var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                    var splitAssetDetailsSearch = [];
                    splitAssetDetailsSearch = assetValueSearch.split('|');
                    updateSeekTimeAndPlay(splitAssetDetailsSearch[0]);
                } else {
                    var assetValueTest = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                    var splitAssetDetails = [];
                    splitAssetDetails = assetValueTest.split('|');
                    updateSeekTimeAndPlay(splitAssetDetails[0]);
                }
            }
        }

    } else if (videoDivVisible) {
       /* videoFileId.setAttribute("controls", "controls")
        setTimeout(function() {
            videoFileId.removeAttribute("controls");
        }, 1000);
        if (videoFileId.paused) {
            videoFileId.play();
        } else {
            flagValuePause = 1;
            videoFileId.pause();
        }*/
    } else if (searchDivVisible == true) {
        if (searchBorder) {
            flagSearchDescribe = 1;
            var assetValueTest = $('#SearchList_' + searchCountAsset).attr("title");
            var splitAssetDetails = [];
            splitAssetDetails = assetValueTest.split('|');
            $("#divSearch").fadeOut();
            $(".preloaderContDescribe").fadeIn();
            $("#landingPageSearch").fadeIn();
            $('#landing_BackgroundImageSearch').css({
                'background-image': "url(" + splitAssetDetails[6] + ")"
            });
         
              if(Store.getItem(userId) == null ){
                        createDataBase();
            }
            else{
                 displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
           }
        } else {
            var activeId = $('.searchCont_active').attr('id');
            if (activeId == "divKeyBoardBackBtn") {
                $("#divSearch").fadeOut();
                $("#categorySelectMain").fadeIn();
                $("#landingPage").fadeIn();
            } else {
                var searchData = "";
                $("#ulAssetContsearchDivData").html("");
                searchCountValue = 0;
                var $write = $('#h2AssetContsearchInput');

                var assetVal = $("#" + keyPadCount + "g").attr("title");
                if (assetVal == "delete") {
                    var html = $write.html();
                    $write.html(html.substr(0, html.length - 1));
                }  else if (assetVal == "space") {
                    character = ' ';
                    if($write.html() == "") {
                          //call function

                    }
                    else{
                         $write.html($write.html() + character);
                    }
                   
                } else {
                    character = assetVal;
                    $write.html($write.html() + character);
                }
                var filter = $("#h2AssetContsearchInput").html();

                var carouselArrayNew = jQuery.grep(carouselArray, function(n, i) {
                    if (n.assetName.search(new RegExp(filter, "i")) < 0) {
                        return 0;
                    } else if (filter == "") {} else {
                        searchCountValue++;
                        var searchCountValueId = searchCountValue;
                        if (searchCountValueId == searchCountValue) {
                            var searchLstIdVal = 'SearchList_' + searchCountValue;
                            var carodatacontent = n.assetId + '|' + n.assetName + '|' + n.assetDescription + '|' + n.assetFileDuration + '|' + n.assetDirector + '|' + n.assetCast + '|' + n.assetImageMain;

                             if(n.assetImage == ""){
                                var searchLstIdVal = 'SearchList_' + searchCountValue;
                            var carodatacontent = n.assetId + '|' + n.assetName + '|' + n.assetDescription + '|' + n.assetFileDuration + '|' + n.assetDirector + '|' + n.assetCast + '|' + n.assetImageMain;
                            searchData = searchData + '<li  class="vid" title="' + carodatacontent + '" id="SearchList_' + searchCountValueId + '"  ><img  src="./images/thumb.jpg"  /></li>';
                            }
                            else{
                                 var searchLstIdVal = 'SearchList_' + searchCountValue;
                            var carodatacontent = n.assetId + '|' + n.assetName + '|' + n.assetDescription + '|' + n.assetFileDuration + '|' + n.assetDirector + '|' + n.assetCast + '|' + n.assetImageMain;
                            searchData = searchData + '<li  class="vid" title="' + carodatacontent + '" id="SearchList_' + searchCountValueId + '"  ><img  src="' + n.assetImage + '"  /></li>';
                            }
                             
                           
                            $("#ulAssetContsearchDivData").html(searchData);
                           
                             $("#divsearchAssetCont").css({
                                    marginTop: "0px"});

                        }

                    }
                });
            }

        }

    } else if (settingsDiv) {
        if (settingsClick == 0) {
            $("#divSettingsMain").fadeOut();
            $("#categorySelectMain").fadeIn();
        } else if (settingsClick == 1) {
            navigator.app.exitApp();
        }
    }
}