/*
     Module Name :
     File Name   :      descriptionClickUp.js
    Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 // written by sachin for present.
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      navigation of buttons in description page for up arrow click
     Organisation:      Peafowl inc.
*/
function descriptionUpClick(episodeButtonVisibleUp, resumeButtonVisibleUp, stat) {

    if (episodeButtonVisibleUp && stat == 0 && resumeButtonVisibleUp!=true) {
          if(isEpisodesBtnVisible){
              if (episodeClick > 1) {
            if (episodeClick == 4) {
                if($("#episode_action"+episodeClick).hasClass("star_active1")){
                  $("#episode_action" + episodeClick).removeClass("star_active1");
                  $("#starRateMainId").html("Rate this title");
                  
                   $("#starRateMainId").removeClass("starRateMain");
                 $("#episode_action"+episodeClick).addClass("rating");
                 $("#episode_action" + episodeClick).css({
                    color: 'white'
                });
            }
            else{
                 $("#episode_action" + episodeClick).removeClass("episode_active1");
                $("#episode_action" + episodeClick).css({
                    color: 'white'
                });
            }
               
            }
            if (episodeClick == 3) {
               
                 $("#episode_action" + episodeClick).removeClass("episode_active1");
                $("#episode_action" + episodeClick).css({
                    color: 'white'
                });
            }
            if (episodeClick == 2) {
                // if(flagButtonMovement == 0){

                if (flagEnterVariable == 1) {
                    //alert('test in upclick***');
                    $("#episode_action" + episodeClick).removeClass("addlist_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'white'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('flagRem 1');
                            $("#episode_action" + episodeClick).removeClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        } else {
                            //alert('flagRem 0');

                            $("#episode_action" + episodeClick).removeClass("addlist_active1");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        }

                    } else {
                        if (flagListAddRemove == 0) {
                            //alert('flagRem 0 **');
                            $("#episode_action" + episodeClick).removeClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        }

                    }

                }
                // }

            }
            if (flagButtonMovement == 0) {
                if (flagListAddRemove == 0)
                    episodeClick--;
            }

            if (episodeClick == 1) {
                if (flagListAddRemove == 0) {
                    //alert('test in up '+episodeClick);
                    $("#episode_action" + episodeClick).addClass("ps1_landingPage_active");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                }

            }

            if (episodeClick == 2) {
                if (flagEnterVariable == 1) {
                    //alert('test in upclick');
                    $("#episode_action" + episodeClick).addClass("addlist_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('test in upclick#####');
                            $("#episode_action" + episodeClick).addClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        } else {
                            //alert('test in upclick%%%%%%%%');
                            $("#episode_action" + episodeClick).addClass("addlist_active1");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        }

                    } else {
                        //alert('test in upclick^^^^^^^');
                        $("#episode_action" + episodeClick).addClass("addlist_active");
                        $("#episode_action" + episodeClick).css({
                            color: 'black'
                        });
                    }

                }

            }
            if(episodeClick == 3){
                 $("#episode_action" + episodeClick).addClass("episode_active1");
                $("#episode_action" + episodeClick).css({
                    color: 'black'
                });
            }
        }
          }
            else{
                  if (episodeClick > 1) {
            if (episodeClick == 3) {
                if($("#episode_action"+episodeClick).hasClass("star_active1")){
                  $("#episode_action" + episodeClick).removeClass("star_active1");
                  $("#starRateMainId").html("Rate this title");
                  
                   $("#starRateMainId").removeClass("starRateMain");
                 $("#episode_action"+episodeClick).addClass("rating");
                 $("#episode_action" + episodeClick).css({
                    color: 'white'
                });
            }
            else{
                 $("#episode_action" + episodeClick).removeClass("episode_active1");
                $("#episode_action" + episodeClick).css({
                    color: 'white'
                });
            }
               
            }
            if (episodeClick == 2) {
                // if(flagButtonMovement == 0){

                if (flagEnterVariable == 1) {
                    //alert('test in upclick***');
                    $("#episode_action" + episodeClick).removeClass("addlist_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'white'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('flagRem 1');
                            $("#episode_action" + episodeClick).removeClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        } else {
                            //alert('flagRem 0');

                            $("#episode_action" + episodeClick).removeClass("addlist_active1");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        }

                    } else {
                        if (flagListAddRemove == 0) {
                            //alert('flagRem 0 **');
                            $("#episode_action" + episodeClick).removeClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        }

                    }

                }
                // }

            }
            if (flagButtonMovement == 0) {
                if (flagListAddRemove == 0)
                    episodeClick--;
            }

            if (episodeClick == 1) {
                if (flagListAddRemove == 0) {
                    //alert('test in up '+episodeClick);
                    $("#episode_action" + episodeClick).addClass("ps1_landingPage_active");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                }

            }

            if (episodeClick == 2) {
                if (flagEnterVariable == 1) {
                    //alert('test in upclick');
                    $("#episode_action" + episodeClick).addClass("addlist_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('test in upclick#####');
                            $("#episode_action" + episodeClick).addClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        } else {
                            //alert('test in upclick%%%%%%%%');
                            $("#episode_action" + episodeClick).addClass("addlist_active1");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        }

                    } else {
                        //alert('test in upclick^^^^^^^');
                        $("#episode_action" + episodeClick).addClass("addlist_active");
                        $("#episode_action" + episodeClick).css({
                            color: 'black'
                        });
                    }

                }

            }
        }
            }
      
    } else if (resumeButtonVisibleUp && stat == 0) {
        if (episodeClick > 1) {
            if (episodeClick == 2) {
                $("#episode_action1").removeClass("ps1_landingPage_active");
                $("#episode_action1").css({
                    color: 'white'
                });
            }
            if (episodeClick == 3) {
                if (flagEnterVariable == 1) {
                    $("#episode_action2").removeClass("addlist_active1");
                    $("#episode_action2").css({
                        color: 'white'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('flagRem 1');
                            $("#episode_action2").removeClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        } else {
                            //  alert('flagRem 0');
                            $("#episode_action2").removeClass("addlist_active1");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        }

                    } else {
                        if (flagListAddRemove == 0) {
                            $("#episode_action2").removeClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        }

                    }

                }

            }
            if(episodeClick == 4){

               // episode_action3
                 $("#episode_action" + (episodeClick-1)).html("");
                  $("#episode_action" + (episodeClick-1)).html("Rate this title");
                  $("#episode_action" + (episodeClick-1)).addClass("rating");
                  $("#episode_action" + (episodeClick-1)).removeClass("star_active1 addlist_active1");
                  $("#episode_action" + (episodeClick-1)).removeClass("addlist_active1");
                   $("#episode_action" + (episodeClick-1)).css({
                    color: 'white'
                });
                 //$("#episode_action" + (episodeClick-1)).removeClass("rating");
                 //star_active1 Rate this title addlist_active1
                /*  $("#starRateMainId").html("Rate this title");
                  star_active1 rating addlist_active1
                   $("#starRateMainId").removeClass("starRateMain");
                 $("#episode_action"+(episodeClick-1)).addClass("rating");
                 $("#episode_action" + (episodeClick-1)).css({
                    color: 'white'
                });*/


            }
            if (flagButtonMovement == 0) {
                if (flagListAddRemove == 0)
                    episodeClick--;
            }
            if (episodeClick == 1) {
                if (flagListAddRemove == 0) {
                    $("#episode_actionResume").addClass("ps1_landingPageResume_active");
                    $("#episode_actionResume").css({
                        color: 'black'
                    });
                }

            }

            if (episodeClick == 2) {

                $("#episode_action1").addClass("ps1_landingPage_active");
                $("#episode_action1").css({
                    color: 'black'
                });

            }
            if(episodeClick == 3){
                  if (flagEnterVariable == 1) {
                        $("#episode_action" + (episodeClick-1)).addClass("addlist_active1");
                        $("#episode_action" + (episodeClick-1)).css({
                            color: 'black'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action" + (episodeClick-1)).addClass("addlist_active");
                                $("#episode_action" + (episodeClick-1)).css({
                                    color: 'black'
                                });
                            } else {
                                $("#episode_action" + (episodeClick-1)).addClass("addlist_active1");
                                $("#episode_action" + (episodeClick-1)).css({
                                    color: 'black'
                                });
                            }

                        } else {

                            $("#episode_action" + (episodeClick-1)).addClass("addlist_active");
                            $("#episode_action" + (episodeClick-1)).css({
                                color: 'black'
                            });
                        }
                    }
            }
        }
    } else if ((episodeButtonVisibleUp == true && resumeButtonVisibleUp == true) && stat == 1) {
       
        if(isEpisodesBtnVisible){
            
              if (episodeClick > 1) {
            if (episodeClick == 2) {
                $("#episode_action1").removeClass("ps1_landingPage_active");
                $("#episode_action1").css({
                    color: 'white'
                });
            }
            if (episodeClick == 3) {
                if (flagEnterVariable == 1) {
                    $("#episode_action2").removeClass("addlist_active1");
                    $("#episode_action2").css({
                        color: 'white'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('flagRem 1');
                            $("#episode_action2").removeClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        } else {
                            //  alert('flagRem 0');
                            $("#episode_action2").removeClass("addlist_active1");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        }

                    } else {
                        if (flagListAddRemove == 0) {
                            $("#episode_action2").removeClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        }

                    }

                }

            }
            if (episodeClick == 4) {
                $("#episode_action3").removeClass("episode_active1");
                $("#episode_action3").css({
                    color: 'white'
                });
            }
            if(episodeClick == 5){
               
                 $("#episode_action" + (episodeClick-1)).removeClass("star_active1");
                  $("#starRateMainId").html("Rate this title");
                  
                   $("#starRateMainId").removeClass("starRateMain");
                 $("#episode_action"+(episodeClick-1)).addClass("rating");
                 $("#episode_action" + (episodeClick-1)).css({
                    color: 'white'
                });
            }
            if (flagButtonMovement == 0) {
                if (flagListAddRemove == 0)
                    episodeClick--;
            }
            if (episodeClick == 1) {
                if (flagListAddRemove == 0) {
                    $("#episode_actionResume").addClass("ps1_landingPageResume_active");
                    $("#episode_actionResume").css({
                        color: 'black'
                    });
                }

            }

            if (episodeClick == 2) {

                $("#episode_action1").addClass("ps1_landingPage_active");
                $("#episode_action1").css({
                    color: 'black'
                });

            }
            if (episodeClick == 3) {
                if (flagEnterVariable == 1) {
                    //alert('test in upclick');
                    $("#episode_action2").addClass("addlist_active1");
                    $("#episode_action2").css({
                        color: 'black'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('test in upclick#####');
                            $("#episode_action2").addClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'black'
                            });
                        } else {
                            //alert('test in upclick%%%%%%%%');
                            $("#episode_action2").addClass("addlist_active1");
                            $("#episode_action2").css({
                                color: 'black'
                            });
                        }

                    } else {
                        //alert('test in upclick^^^^^^^');
                        $("#episode_action2").addClass("addlist_active");
                        $("#episode_action2").css({
                            color: 'black'
                        });
                    }

                }

            }
            if(episodeClick == 4){
                 $("#episode_action"+(episodeClick-1)).addClass("episode_active1");
                $("#episode_action3").css({
                    color: 'black'
                });
            }
        }
        }
        else{
             if (episodeClick > 1) {
            if (episodeClick == 2) {
                $("#episode_action1").removeClass("ps1_landingPage_active");
                $("#episode_action1").css({
                    color: 'white'
                });
            }
            if (episodeClick == 3) {
                if (flagEnterVariable == 1) {
                    $("#episode_action2").removeClass("addlist_active1");
                    $("#episode_action2").css({
                        color: 'white'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('flagRem 1');
                            $("#episode_action2").removeClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        } else {
                            //  alert('flagRem 0');
                            $("#episode_action2").removeClass("addlist_active1");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        }

                    } else {
                        if (flagListAddRemove == 0) {
                            $("#episode_action2").removeClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        }

                    }

                }

            }
            if (episodeClick == 4) {
                $("#episode_action3").removeClass("episode_active1");
                $("#episode_action3").css({
                    color: 'white'
                });
            }
            if(episodeClick == 5){
                 $("#episode_action" + episodeClick).removeClass("star_active1");
                  $("#starRateMainId").html("Rate this title");
                  
                   $("#starRateMainId").removeClass("starRateMain");
                 $("#episode_action"+episodeClick).addClass("rating");
                 $("#episode_action" + episodeClick).css({
                    color: 'white'
                });
            }
            if (flagButtonMovement == 0) {
                if (flagListAddRemove == 0)
                    episodeClick--;
            }
            if (episodeClick == 1) {
                if (flagListAddRemove == 0) {
                    $("#episode_actionResume").addClass("ps1_landingPageResume_active");
                    $("#episode_actionResume").css({
                        color: 'black'
                    });
                }

            }

            if (episodeClick == 2) {

                $("#episode_action1").addClass("ps1_landingPage_active");
                $("#episode_action1").css({
                    color: 'black'
                });

            }
            if (episodeClick == 3) {
                if (flagEnterVariable == 1) {
                    //alert('test in upclick');
                    $("#episode_action2").addClass("addlist_active1");
                    $("#episode_action2").css({
                        color: 'black'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('test in upclick#####');
                            $("#episode_action2").addClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'black'
                            });
                        } else {
                            //alert('test in upclick%%%%%%%%');
                            $("#episode_action2").addClass("addlist_active1");
                            $("#episode_action2").css({
                                color: 'black'
                            });
                        }

                    } else {
                        //alert('test in upclick^^^^^^^');
                        $("#episode_action2").addClass("addlist_active");
                        $("#episode_action2").css({
                            color: 'black'
                        });
                    }

                }

            }
            if(episodeClick == 4){
                 $("#episode_action"+episodeClick).addClass("episode_active1");
                $("#episode_action3").css({
                    color: 'white'
                });
            }
        }
        }
       
    } else {
        if (episodeClick > 1) {
           
            if (episodeClick == 2) {
                if (flagEnterVariable == 1) {
                    //alert('in enter flag');
                    $("#episode_action2").removeClass("addlist_active1");
                    $("#episode_action2").css({
                        color: 'white'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('flag rem 1');
                            $("#episode_action" + episodeClick).removeClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        } else {
                            //alert('flage rem 0');
                            $("#episode_action" + episodeClick).removeClass("addlist_active1");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        }

                    } else {
                        //alert('else rem 0');
                        if (flagListAddRemove == 0) {
                            $("#episode_action" + episodeClick).removeClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        }

                    }

                }

            }
            if(episodeClick == 3){
                 $("#episode_action" + episodeClick).removeClass("star_active1");
                  $("#starRateMainId").html("Rate this title");
                  
                   $("#starRateMainId").removeClass("starRateMain");
                 $("#episode_action"+episodeClick).addClass("rating");
                 $("#episode_action" + episodeClick).css({
                    color: 'white'
                });
            }
            if (flagButtonMovement == 0) {
                if (flagListAddRemove == 0)
                    episodeClick--;
            }
            if (episodeClick == 1) {
                if (flagListAddRemove == 0) {
                    $("#episode_action" + episodeClick).addClass("ps1_landingPage_active");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                }

            }

            if (episodeClick == 2) {
                if (flagEnterVariable == 1) {
                    $("#episode_action" + episodeClick).addClass("addlist_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            //alert('test 1');
                            $("#episode_action" + episodeClick).addClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        } else {
                            $("#episode_action" + episodeClick).addClass("addlist_active1");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        }

                    } else {
                        //alert('test');
                        $("#episode_action" + episodeClick).addClass("addlist_active");
                        $("#episode_action" + episodeClick).css({
                            color: 'black'
                        });
                    }

                }

            }
        }
    }
}