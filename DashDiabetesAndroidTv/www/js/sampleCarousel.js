    
/*
     Module Name :
     File Name   :      carousel.js
     Project     :      dev.damedashstudios.com
     Copyright (c)      Damedash Studios.
     author      :      Hari Kumar.
     author      :      Sachin Singh J.
     license     :
     version     :      0.1.0 // written by sachin for present.
     Created on  :      8 august 12:26 pm
     Last modified on:  8 august
     Description :      Getting data for carousels and category div i.e, the thumb image and particular thumb
                        detail.
     Organisation:      Peafowl inc.
*/
//Get my list
function getMyList(){
  var storage = window.localStorage;
     $.ajax({
      type: "POST",
      url: URL_LINK+"/fetchMyList",
      data: JSON.stringify({"fetchMyList":{ "userId": storingIn, "id":deviceId,"userType":"device"}}),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(data){
        var imgData=data.myList;
        if (imgData.length > 0) {
            var HTMLContent = "";
            var MyListCourosalID = mylistname.replace(/ /g, '');
            
            var i;
            var HTMLContent = '<div class="movieCategory_landingPage" id="cate_'+cnt+'"><h5>' + mylistname + '</h5><div id="' + MyListCourosalID + '"  class="als-container"><div class="als-viewport"> <ul id="ulThumbsMyList" class="als-wrapper ">';
              for (i = 0; i < imgData.length; i++) {
                if(i==0){
                  var assetidcnt = imgData[i].data.id+'|'+imgData[i].data.name+'|'+imgData[i].data.description+'|'+imgData[i].data.file_duration+'|'+imgData[i].data.metadata.main_carousel_image_url+'|'+imgData[i].data.metadata.release_date+'|'+imgData[i].data.metadata.director+'|'+imgData[i].data.metadata.cast;
 
                }
             var carodatacontent = imgData[i].data.id+'|'+imgData[i].data.name+'|'+imgData[i].data.description+'|'+imgData[i].data.file_duration+'|'+imgData[i].data.metadata.main_carousel_image_url+'|'+imgData[i].data.metadata.release_date+'|'+imgData[i].data.metadata.director+'|'+imgData[i].data.metadata.cast;

              var HTMLContent = HTMLContent + '<li class="als-item" title="'+carodatacontent+'"><img  src="'+imgData[i].data.metadata.movie_art+'" width="356px" height="199px" /></li>';

            } // for loop close 
            var HTMLContent = HTMLContent +'</ul></div><input type="hidden" name="assetid'+cnt+'" id="assetid'+cnt+'" value="'+assetidcnt+'"><input type="hidden" name="leftid'+cnt+'" id="leftid'+cnt+'" value="0"></div></div>'; 
            //alert(HTMLContent);
            $("#thumbsList").append(HTMLContent);
            $('#'+MyListCourosalID).als({
              visible_items: 5,
              scrolling_items: 1,
              orientation: "horizontal",
              circular: "yes",
              autoscroll: "no",
              interval: 5000,
              speed: 25,
              easing: "linear",
              direction: "right",
              start_from: 0
            });
           
             cnt++;
           
             if(cnt==1)
             {

                 var carodataFirstcontent = imgData[0].data.id+'|'+imgData[0].data.name+'|'+imgData[0].data.description+'|'+imgData[0].data.file_duration+'|'+imgData[0].data.metadata.main_carousel_image_url+'|'+imgData[0].data.metadata.release_date+'|'+imgData[0].data.metadata.director+'|'+imgData[0].data.metadata.cast+'|'+imgData[0].data.tv_show;
                  assetDetailsSplit(carodataFirstcontent);
             }
             mylistlength=1;
          }
        },
          error: function(jqXHR, textStatus, errorThrown) {
       
          },
          complete: function() {
            recentwatched();
        
          }                    
    });
}


// Get Recently Watched
function recentwatched(){
  var storage = window.localStorage;
     $.ajax({
      type: "POST",
      url: URL_LINK+"/fetchRecentlyWatched",
      data: JSON.stringify({"fetchRecentlyWatched":{ "userId": storingIn, "id":deviceId, "userType":"device"}}),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(data){
        var imgData=data.recentlyWatched;
        if (imgData.length > 0) {
            var HTMLContent = "";
            var RecentCourosalID = recentlywatchedname.replace(/ /g, '');
            
            var i;
            var HTMLContent = '<div class="movieCategory_landingPage" id="cate_'+cnt+'"><h5>' + recentlywatchedname + '</h5><div id="' + RecentCourosalID + '"  class="als-container"><div class="als-viewport"> <ul id="ulThumbsMyList" class="als-wrapper ">';
            for (i = 0; i < imgData.length; i++) {

               if(i==0){
                var assetidcnt = imgData[i].data.id+'|'+imgData[i].data.name+'|'+imgData[i].data.description+'|'+imgData[i].data.file_duration+'|'+imgData[i].data.metadata.main_carousel_image_url+'|'+imgData[i].data.metadata.release_date+'|'+imgData[i].data.metadata.director+'|'+imgData[i].data.metadata.cast;

                
                }

                var carodatacontent = imgData[i].data.id+'|'+imgData[i].data.name+'|'+imgData[i].data.description+'|'+imgData[i].data.file_duration+'|'+imgData[i].data.metadata.main_carousel_image_url+'|'+imgData[i].data.metadata.release_date+'|'+imgData[i].data.metadata.director+'|'+imgData[i].data.metadata.cast;
              

              var HTMLContent = HTMLContent + '<li class="als-item" title="'+carodatacontent+'"><img  src="'+imgData[i].data.metadata.movie_art+'" width="356px" height="199px" /></li>';
            } // for loop close 
            var HTMLContent = HTMLContent +'</ul></div><input type="hidden" name="assetid'+cnt+'" id="assetid'+cnt+'" value="'+assetidcnt+'"><input type="hidden" name="leftid'+cnt+'" id="leftid'+cnt+'" value="0"></div></div>'; 
            //alert(HTMLContent);
            $("#thumbsList").append(HTMLContent);
            $('#'+RecentCourosalID).als({
              visible_items: 5,
              scrolling_items: 1,
              orientation: "horizontal",
              circular: "yes",
              autoscroll: "no",
              interval: 5000,
              speed: 25,
              easing: "linear",
              direction: "right",
              start_from: 0
            });
            cnt++;
           
            if(cnt==1)
            {
                var carodataFirstcontent = imgData[0].data.id+'|'+imgData[0].data.name+'|'+imgData[0].data.description+'|'+imgData[0].data.file_duration+'|'+imgData[0].data.metadata.main_carousel_image_url+'|'+imgData[0].data.metadata.release_date+'|'+imgData[0].data.metadata.director+'|'+imgData[0].data.metadata.cast+'|'+imgData[0].data.tv_show;
                 assetDetailsSplit(carodataFirstcontent);
            }
          recentlength=1;
          }
        },
          error: function(jqXHR, textStatus, errorThrown) {
            
          },
          complete: function() {
             
           getCarouselNames();
          }                    
    });
}



// Get Carousel Names
function getCarouselNames() {
    //$("#loading").show();
    var storage = window.localStorage;
// alert('getCarousel '+storage.getItem(keyDeviceId));
    $.ajax({
        url: URL_LINK + "/getCarousels/",
        type: "POST",
        data: JSON.stringify({"getCarousels":{"id":deviceId, "userType":"device"}}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            if (data.length > 0) {
                var i;
                var classactive;
                for (i = 0; i < data.length; i++) {
                    if(i==0){
                         classactive = 'cate_buttonactive';
                    }else{
                        classactive = '';
                    }
                    $("#cateList").append('<li class="'+classactive+'" id="Catedata_' + i + '">' + data[i].carousel_name + '</li>');
                    
                    // $("#searchCat").append('<li  id="searchdata_' + i + '">' + data[i].carousel_name + '</li>');

                    NavItems.push(data[i].carousel_name);
                    CarouselItems.push(data[i].carousel_name);
                    if (data.length == i + 1) {
                      getCarouselItem(NavItems);
                    }
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
         // alert(data);
         // alert('test'+URL_LINK + "/getCarousels/");
        },
        complete: function() {
          //  alert(NavItems.length);
         // alert(data);
         // alert('test'+URL_LINK + "/getCarousels/");

        }
    });
}



// Get Get Asset Id With Asset Names 
function getCarouselItem(NavItems) { 
  var storage = window.localStorage;
if (NavItems.length > 0) {
        var id = NavItems[0];
        $.ajax({
            url: URL_LINK+ "/getCarouselData/",
            type: "POST",
            data: JSON.stringify({"getCarouselData":{"name": id, "id":deviceId, "userType":"device"}}),
            contentType: "application/json; charset=utf-8",
            success: function(imgData) { 
                if (imgData[id].length > 0) {
                    var HTMLContent = "";
                     var searchData = "";
                    var CourosalID = id.replace(/ /g, '');
                    var i;
                    var HTMLContent = '<div class="movieCategory_landingPage" id="cate_'+cnt+'"><h5>' + id + '</h5><div id="' + CourosalID + '"  class="als-container"><div class="als-viewport"> <ul id="ulThumbsMyList" class="als-wrapper ">';
                  
                    
                    var categorydata = '<div  id="Catecomplete_' + categorycount + '" class="thumbsList hidedivclass"><ul >';
                    
                   
                    for (i = 0; i < imgData[id].length; i++) {
                          searchCount++;
                        if( imgData[id][i].metadata.movie_art!=null )
                        {
                          if(i==0){
                          var assetidcnt = imgData[id][i].id+'|'+imgData[id][i].name+'|'+imgData[id][i].description+'|'+imgData[id][i].file_duration+'|'+imgData[id][i].metadata.main_carousel_image_url+'|'+imgData[id][i].metadata.release_date+'|'+imgData[id][i].metadata.director+'|'+imgData[id][i].metadata.cast;
                          }
                           catLstId='CategoryList_'+categorycount+'_'+i;
                           searchLstId='SearchList_'+searchCount;
                           carouselArray.push({"assetId":imgData[id][i].id,
                             "assetName":imgData[id][i].name,
                             "assetDescription":imgData[id][i].description,
                             "assetFileDuration":imgData[id][i].file_duration,
                              "assetDirector":imgData[id][i].metadata.director,
                              "assetCast":imgData[id][i].metadata.cast,
                            "assetImage":imgData[id][i].metadata.movie_art,
                            "assetImageMain":imgData[id][i].metadata.main_carousel_image_url});
                          // alert('JSON data '+JSON.stringify(carouselArray));
                          var carodatacontent = imgData[id][i].id+'|'+imgData[id][i].name+'|'+imgData[id][i].description+'|'+imgData[id][i].file_duration+'|'+imgData[id][i].metadata.main_carousel_image_url+'|'+imgData[id][i].metadata.release_date+'|'+imgData[id][i].metadata.director+'|'+imgData[id][i].metadata.cast;

                          var HTMLContent = HTMLContent + '<li class="als-item" title="'+carodatacontent+'"><img  src="'+imgData[id][i].metadata.movie_art+'" width="356px" height="199px" /></li>';

                          /*var categorydata = categorydata + '<li id="'+catLstId+'"  title="'+carodatacontent+'"><h1>"'+imgData[id][i].id+'"<h1/></li>';*/
                          var categorydata = categorydata + '<li id="'+catLstId+'"  title="'+carodatacontent+'"><img  src="'+imgData[id][i].metadata.movie_art+'" width="356px" height="199px" /></li>';
                          
                        /*   searchData = searchData + '<li   style="display:none" class="vid " id="'+searchLstId+'"  title="'+carodatacontent+'"><img  src="'+imgData[id][i].metadata.movie_art+'"  /></li>';*/
                          

                        }
                          
                    } // for loop close 
                    var HTMLContent = HTMLContent +'</ul></div><input type="hidden" name="assetid'+cnt+'" id="assetid'+cnt+'" value="'+assetidcnt+'"><input type="hidden" name="leftid'+cnt+'" id="leftid'+cnt+'" value="0"></div></div>'; 
                    
                     var categorydata = categorydata +'</ul></div>'; 
                     
                      //alert(HTMLContent);
                    $("#thumbsList").append(HTMLContent);
      
                    $('#'+CourosalID).als({
                      visible_items: 5,
                      scrolling_items: 1,
                      orientation: "horizontal",
                      circular: "yes",
                      autoscroll: "no",
                      interval: 5000,
                      speed: 25,
                      easing: "linear",
                      direction: "right",
                      start_from: 0
                    });
                    $("#CategroyDataDiv").append(categorydata);
                     cnt++;
                     
                     categorycount++;  
                    if(cnt==1)
                    {
                       var carodataFirstcontent = imgData[id][0].id+'|'+imgData[id][0].name+'|'+imgData[id][0].description+'|'+imgData[id][0].file_duration+'|'+imgData[id][0].metadata.main_carousel_image_url+'|'+imgData[id][0].metadata.release_date+'|'+imgData[id][0].metadata.director+'|'+imgData[id][0].metadata.cast+'|'+imgData[id][0].tv_show;
                       assetDetailsSplit(carodataFirstcontent);
                    }
          }
          
            },
            error: function(jqXHR, textStatus, errorThrown) {
            },
            complete: function() {
                NavItems.splice($.inArray(NavItems[0], NavItems), 1);
                getCarouselItem(NavItems);
                if(NavItems.length == 0){
                  $(".preloaderContDescribe").fadeOut(); 
                }
               
            }
        });
      }
}




