/*
     Module Name :
     File Name   :      rightClick.js
     Project     :      dev.damedashstudios.com
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      0.0.12 
     Created on  :      9th october 
     Last modified on:  30th November
     Description :      right click functionality for fire tv remote control
     Organisation:      Peafowl inc.
*/
function rightClickFunction() {
    var isThumbListVisibleTrue = $('#thumbsList').is(':visible');
    var ismenuDiv = $('#menuDiv').is(':visible');
    var catVisible = $('#Catecomplete_' + countDownClickVar).is(':visible');
    var episodesDivVisible = $('#episodeMainDiv').is(':visible');
    var catSelectVisible = $('#categorySelectMain').is(':visible');
    var assetCountValue = categoryValueCount + categoryDownClick;
    var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
    var searchDivVisible = $('#divSearch').is(':visible');
    var searchContVisible = $('.vid').is(':visible');
    if (isThumbListVisibleTrue) {
        //$.fn.als("next", countDownClickVarLanding);
        
		 $("#reg"+countDownClickVarLanding).addClass('snext');
       if(slickVal == countDownClickVarLanding){
        changeSlick = 1;
         var $carousel = $('.snext');
        $carousel.slick('slickNext');
       }
    }
else if(slideShower == 1){

 $("#reg_Slide").addClass('snext');

       // $("#reg"+countDownClickVarLanding).slick('unslick');
       if(slickVal == countDownClickVarLanding){
        
        changeSlick = 1;
         var $carousel = $('.snext');
        $carousel.slick('slickNext');
       }

}
     else if (ismenuDiv) {
        
        //categoryValueCount++;
        //categoryDownClick++;
        $("#menuDiv").fadeOut();
        $("#CategroyDataDiv").css({
            marginRight: "-1%",
            width: "98%"
        });
        $("#image1").fadeIn();
        $(".categoryData .thumbsList li").css("width", "18%");
        $("#descTxtCat").fadeIn();
        assetDetailsSplitCat(assetDataValues);
        //$('#catBackgroundImage').css("background-image", "url("+assetDataArray[4]+")"); 

    } else if (catVisible) {
        
        categoryValueCount++;
        categoryDownClick++;


        var assetVal = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
        if (categoryValueCount > 4 || assetVal == null) {
            categoryDownClick--;
            categoryValueCount--;
            flagRightClickCat = 1;
        }
        if (assetVal != null && flagRightClickCat == 0) {
            if (categoryValueCount <= 4) {
                assetDetailsSplitCat(assetVal);
                // $('#catBackgroundImage').css("background-image", "url("+assetDataArray[4]+")"); 
                rightMov = marginValue + rightMov;
                // $("#image1").css({ marginLeft : rightMov+"px"});
                $("#image1").animate({
                    marginLeft: rightMov + "px"
                }, 50);

            }
        } else {
            flagRightClickCat = 0;
            rightAssetFound = 1;
        }

    } else if (episodesDivVisible) {
       
        $("#episodeDescription").addClass("episodeInactive");
        $(".borderEpisodes").fadeIn();
        $("#episodeList").removeClass("episodeInactive");
    } else if (catSelectVisible) {
        var divCount = $(".icon_sel").length
            /*  if(catSelectCount<$('ul#menu li').length){
                  $("#"+catSelectCount).removeClass("categoriesDiv_active");
                  catSelectCount++;
                  $("#"+catSelectCount).addClass("categoriesDiv_active")
              }*/
        if (catSelectCount < divCount) {
           
            $("#" + catSelectCount).removeClass("active_sel");
            catSelectCount++;
            $("#" + catSelectCount).addClass("active_sel")
        }
        if (catSelectCount == 1) {
            $("#txtDescribe").html("Search for TV shows, movies, categories and music");
        } else if (catSelectCount == 2) {
            $("#txtDescribe").html("Browse categories");
        } else if (catSelectCount == 3) {
            $("#txtDescribe").html("Update account settings");
        } else if (catSelectCount == 4) {
            $("#txtDescribe").html("Exit Dash Diabetes Network");
        }
    } else if (searchDivVisible) {
       
        if (keyPadCount < 38) {

            if (keyPadCount == 2 && countDownKeyPad == 1) {
                //countDownKeyPad++;

                if (searchContVisible == true) {

                    $("#divimageBorderSearch").show();
                }
            } else {

                if (keyPadCount == keyPadValue) {
                    if (searchContVisible) {
                        $("#" + keyPadCount + "g").removeClass("searchCont_active");
                         $("#" + keyPadCount + "g").css({
                        color: 'white'
                    });
                        searchCountAsset++;
                        var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        if (assetValSearch != null) {
                            if (searchCountAsset == 1) {
                                $("#divimageBorderSearch").show();
                                var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
                            } else if (searchRightClick < 2) {
                                $("#divimageBorderSearch").show();
                                searchRightClick++;
                                var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
                                rightMovSearch = marginValueSearch + rightMovSearch;
                                $("#divimageBorderSearch").animate({
                                    marginLeft: rightMovSearch + "px"
                                }, 50);
                            } else {

                                searchCountAsset--;

                            }
                        }
                    }
                } else {

                    $("#" + keyPadCount + "g").removeClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'white'
                    });
                    keyPadCount++;
                    $("#" + keyPadCount + "g").addClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'black'
                    });
                    var assetVal = $("#" + keyPadCount + "g").attr("title");
                }


            }


        }
    }
     else if (isDescriptionDivVisible) {
        if ($("#episode_action4").hasClass("star_active1") || $("#episode_action3").hasClass("star_active1") || $("#episode_action2").hasClass("star_active1")) {
            var divCount = $('#starRateMainId').children('div').length;
            if (startRateCount < divCount) {
                startRateCount++;
                $('#rating' + startRateCount).addClass('star_active').removeClass('star');
               
            }

        }
    }
}