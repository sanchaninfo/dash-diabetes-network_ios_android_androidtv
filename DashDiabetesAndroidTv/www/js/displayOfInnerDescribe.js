/*
     Module Name :
     File Name   :      displayOfInnerDescribe.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 // written by sachin for present.
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      function for calling getAssetData to get asset details and display them in description
                        page .
     Organisation:      Peafowl inc.
*/
function displayOfInnerDescribedPage(userIdValue, assetId) {
 var storage = window.localStorage;

   $.ajax({
                    type: "POST",
                    //url: URL_LINK + "/getAssetRating",
                    url: ServerLamdaUrl + "getassetrating?assetId="+assetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (dataRate) {
                        var data = dataRate.result;
                       // addOrRemoveAssetToList(data.assetId, data, data.userData[0].myList, flagNextAsset);
                       userratingsum = data[0].userassetrating;
                       startRateCount = data[0].userassetrating;
                       avgAssetRating = data[0].averagerating;
                       
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                        

                    }
                });


     $.ajax({
        type: "POST",
       // url: URL_LINK + "/getAssetData",
        url: ServerLamdaUrl + "getassetdata?assetId="+assetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(dataValue) {
            
            var data = dataValue.result;
           
             var stringValue = data.assetName;
             stringValue = stringValue.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
            globalassetId = assetId;
            assetDataObj = data;
            containsObj = data.contains;
            tvShowStatus = data.tv_show;
            watchedVideoFlag = data.userData[0].watchedVideo;
            myListPresentFlag = data.userData[0].myList;
            assetNameValue = stringValue;
            assetImageValue = data.landscape_url_500x281;
            assetDescriptionValue = data.description;
            assetDurationValue = data.duration;
            assetYearValue = data.releaseDate;
            seasonNumValue = data.seasonCategoryNo;
            episodeNumValue = data.episodeNo;
            nextAssetMyList = data.userData[0].myList;
            monetize = data.monetize;
            monetize_type = data.monetizeType;
            monetize_label = data.monetizeLabel;
            monetize_data_source_url = data.monetize_data_source_url;
            if (data.m3u8_url != null && data.m3u8_url != "") {
                videoUrlAsset = data.m3u8_url;

            } else {
                videoUrlAsset = data.mp4_url;
            }
        if(assetNextFlag == 0){
          
             if (containsObj && containsObj.length>1) {
                if (myListPresentFlag == true) {

                    if (watchedVideoFlag > 0) {
                        descriptionDisplayValue = 7;
                        addingToMyListDisplayFun(assetImageValue, assetNameValue, assetDescriptionValue, assetDurationValue, assetYearValue, descriptionDisplayValue);
                        flagRecentTest = 1;

                    } else {

                        descriptionDisplayValue = 5;
                        addingToMyListDisplayFun(assetImageValue, assetNameValue, assetDescriptionValue, assetDurationValue, assetYearValue, descriptionDisplayValue);
                    }
                } else {
                    if (watchedVideoFlag > 0) {

                        descriptionDisplayValue = 8;
                        addingToMyListDisplayFun(assetImageValue, assetNameValue, assetDescriptionValue, assetDurationValue, assetYearValue, descriptionDisplayValue);
                    } else {

                        descriptionDisplayValue = 6;
                        addingToMyListDisplayFun(assetImageValue, assetNameValue, assetDescriptionValue, assetDurationValue, assetYearValue, descriptionDisplayValue);
                    }
                }
            } else {
                if (myListPresentFlag == true) {

                    if (watchedVideoFlag > 0) {

                        descriptionDisplayValue = 1;
                        addingToMyListDisplayFun(assetImageValue, assetNameValue, assetDescriptionValue, assetDurationValue, assetYearValue, descriptionDisplayValue);
                        flagRecentTest = 1;

                    } else {

                        descriptionDisplayValue = 2;
                        addingToMyListDisplayFun(assetImageValue, assetNameValue, assetDescriptionValue, assetDurationValue, assetYearValue, descriptionDisplayValue);
                    }
                } else {

                    if (watchedVideoFlag > 0) {

                        descriptionDisplayValue = 3;
                        addingToMyListDisplayFun(assetImageValue, assetNameValue, assetDescriptionValue, assetDurationValue, assetYearValue, descriptionDisplayValue);
                    } else {

                        descriptionDisplayValue = 4;
                        addingToMyListDisplayFun(assetImageValue, assetNameValue, assetDescriptionValue, assetDurationValue, assetYearValue, descriptionDisplayValue);
                        //addingToMyListDisplayFun(userImageValue,userNameValue,userDescription,userDuration,userListYear,descriptionDisplayValue);
                    }
                }
            }
        }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // alert('error');
        },
        complete: function() {
            
            if(assetNextFlag == 1){
                assetNextFlag = 0;
                if(monetize == "true"){               
                videoPlayWithStore(assetId,0);
                }else{  
                if(videoPlayerInEpisodes == 1){
                    episodeVideoPlay(assetId, videoUrlAsset)
                }else{
                    updateSeekTimeAndPlay(assetId);
                }                 
                
              }
            }

              

        }
    });
 

}