/*
     Module Name :
     File Name   :      episodes.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :      Prakash Ragala.
     license     :
     version     :      1.0.0. 
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November 2016
     Description :      functionality for episodes display.
     Organisation:      Peafowl inc.
*/
function getEpisodedetails(asset_id) {
    var episodeList = '';  //need to declare in constants Js file
    var episodeTitle = '';
    var fixedDiv = '';
    var episodeChoose = '';
    var year = yearOfRelease(assetYearValue);
    $('#episodeList').empty();
    episodeTitle += '<li><h2 class="mainTitle_12">' + assetNameValue + '</h2></li><li><h6><span class="movieDesc_14">' + year + '</span><span class="movieDesc_14"> Season</span></h6></li> <li><p  class="synopsys_category"></p></li>'
    $('#episodeTitle').append(episodeTitle);
    console.log(JSON.stringify(containsObj))
    //$('#episodes_load').html('<span class="loading2_category">Loading...</span>');   
    if (containsObj.length > 0) {
        for (var season = 0; season < containsObj.length; season++) {
            if (seasons.length > 0) {
                flagSeasonPresent = 1;
                for (var i = 0; i < seasons.length; i++) {

                    if (seasons[i] == containsObj.seasonCategoryNo) {
                        flagSeasonPresent = 0;
                        break;
                    }
                }
                if (flagSeasonPresent == 1) {

                    seasons.push(containsObj.seasonCategoryNo);
                }
            } else {

                seasons.push(containsObj.seasonCategoryNo);
            }
            if (season == containsObj.length - 1) {
                seasons.sort(function(a, b) {
                    return a - b
                });
                classactive = 'catActive';
                for (var seasonNumber = 0; seasonNumber < seasons.length; seasonNumber++) {
                    //alert('test '+seasonNumber);
                    var classactive;
                    if (seasonNumber > 0) {
                        classactive = '';
                    }
                    /* episodeChoose += '<li class="activebutton_category ' + classactive + '" id="season_' + seasonNumber + '">Season ' + seasons[seasonNumber] + '<span class="pullright" > </span></li>';*/

                    episode_num = 1;
                    var episodeNumVal = 0;
                    var episodesDivHtml = '<div class="borderEpisodes" style="display:none"> </div><div id="episodeSeason_' + seasonNumber + '" style="display:none" >';
                    while(containsObj.length != episodeNumberPush.length){
                    for (var k = 0; k <= containsObj.length; k++) {
                        for (var j = 0; j < containsObj.length; j++) {
                            if (seasons[seasonNumber] == containsObj.seasonCategoryNo) {
                                if (containsObj[j].episodeNo == episode_num) {
                                    episodeNumVal++;
                                    var catLstId = 'Episode_' + seasonNumber + '_' + episodeNumVal;
                                    if (containsObj[j].m3u8_url != undefined && containsObj[j].m3u8_url != "") {
                                        var carodatacontent = containsObj[j].assetId + '|' + containsObj[j].m3u8_url;
                                    } else {
                                        var carodatacontent = containsObj[j].assetId + '|' + containsObj[j].mp4_url;

                                    }
                                    episodeNumberPush.push(containsObj[j].episodeNo);
                                    episodesDivHtml = episodesDivHtml + '<div id="' + catLstId + '"  title="' + carodatacontent + '"><div class="epi_video_cont"><div class="epi_desc"><h3>Season ' + containsObj[j].seasonCategoryNo + ' : Episode ' + containsObj[j].episodeNo + '</h3></div><div class="epi_video"><img  src=' + containsObj[j].landscape_url_500x281 + ' ></div><div class="epi_video_txt"><div class="episodeHeading"><p> ' + containsObj[j].description.replace(/[^a-zA-Z 0-9]+/g, "") + '</p></div><div class="duration"></div><div class="clearFx"></div></div></div></div>';
                                   // episode_num++;
                                    break;
                                }
                            }
                        }
                        episode_num++;
                    }
                }
                    if(seasons[seasonNumber] == null){
                         episodeChoose += '<li class="activebutton_category ' + classactive + '" id="season_' + seasonNumber + '">Season ' + 0 + '<span class="pullright" >Episodes ' + episodeNumVal + ' </span></li>';
                    }
                   else{
                     episodeChoose += '<li class="activebutton_category ' + classactive + '" id="season_' + seasonNumber + '">Season ' + seasons[seasonNumber] + '<span class="pullright" >Episodes ' + episodeNumVal + ' </span></li>';
                   }

                    episodesDivHtml = episodesDivHtml + '<input type="hidden" name="assetidEpisode" id="assetidEpisode' + seasonNumber + '" value=""></div>';
                    $('#episodeList').append(episodesDivHtml);

                    $("#episodeSeason_0").show();
                    $("#episodeMainDiv").fadeIn();
                    $("#episodeDescription").fadeIn();
                    $('#season_0').addClass('catActive');

                    $("#episodeList").addClass("episodeInactive");
                    //$("#episodeDescription").addClass("episodeInactive");
                }
            }
            $('#episodeButtons').append(episodeChoose);
        }


    } else {
        classactive = 'catActive';
        episodeChoose += '<li class="activebutton_category ' + classactive + '" id="season_0">Season 1<span class="pullright" > </span></li>';
        var episodesDivHtml = '<div class="borderEpisodes"> </div><div id="episodeSeason_0" style="display:none" >';
        //episodeChoose += '<li class="activebutton_category ' + classactive + '" id="season_0">Season 1<span class="pullright" > </span></li>';

        var episodesDivHtml = '<div class="borderEpisodes"> </div><div id="episodeSeason_0" style="display:none" >';

        var catLstId = 'Episode_0_1';
        var carodatacontent = asset_id + '|' + videoUrlAsset;
        episodesDivHtml = episodesDivHtml + '<div id="' + catLstId + '"  title="' + carodatacontent + '"><div class="epi_video_cont"><div class="epi_desc"><h3>S1:1</h3></div><div class="epi_video"><img width="356px" height="199px" src=' + assetImageValue + ' ></div><div class="epi_video_txt"><div class="episodeHeading"><q>Episode 1</q></div><div class="duration"></div><div class="clearFx"></div></div></div></div>'


        episodesDivHtml = episodesDivHtml + '<input type="hidden" name="assetidEpisode" id="assetidEpisode0" value=""></div>';
        $('#episodeList').append(episodesDivHtml);

        $("#episodeSeason_0").show();
        $("#episodeMainDiv").fadeIn();
        $("#episodeDescription").fadeIn();
        $('#season_0').addClass('catActive');
        $('#episodeButtons').append(episodeChoose);
    }
}