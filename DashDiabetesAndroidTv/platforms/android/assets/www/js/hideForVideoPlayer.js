/*
     Module Name :
     File Name   :      hideForVideoPlayer.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      function for hiding div's for showing videoplayer
     Organisation:      Peafowl inc.
*/
function hideForVideoPlay(playerFlag) {
    if (playerFlag == 0) {
        $('#video_player').html('');
        $("#video_player").show();
        $("#descriptionDivDescribe").hide();
        $("#divIDImg").hide();
        if (flagMainDivVisible == 0) {
            if (flagSearchDescribe == 1) {
                $("#landingPageSearch").fadeOut();
            } else {
                $("#landingPage").fadeOut();
            }
        } else {
            $("#catBackground").fadeOut();
        }
        $("#buttonShow").hide();
        $(".ddlogobottom").hide();
        $(".thumbsCont_landingPage").hide();
        $("#title").hide();
        $("#movieDescribe").hide();
        $("#divID").hide();
       // $("#videoLoad").show();
        $("#videoTitle").html(assetNameValue);
        if (seasonNumValue != "" || episodeNumValue != "") {
            $("#videoTitleSub").show();
            $("#videoTitleSub").html("Season  :" + seasonNumValue + " Episode  :" + episodeNumValue);
        }


    } else if (playerFlag == 1) {
        $('#video_player').html('');
        $("#video_player").show();
        $("#episodeMainDiv").fadeOut();
       // $("#videoLoad").show();
        $("#videoTitle").html(assetNameValue);
    }
}