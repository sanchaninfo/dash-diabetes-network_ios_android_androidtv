/**
 *
  Module Name :      
  File Name   :      common.js
  Project     :      dev.damedashstudios.com
  Copyright (c) Damedash Studios.
  author      :      Siddartha.
  author      :      
  license     :   
  version     :      1.0.0 // written by siddartha for present. 
  Created on  :      6 august 12:20 pm
  Last modified on:  30th November 
  Description :      This file contains url links and global variables which are used through out the application.  
  Organisation:      Peafowl inc.
 */

/*var APP_ENV  = "PROD";

var URL_LINK = "http://dev.damedashstudios.com";

if(APP_ENV == "PROD")
  URL_LINK = "http://www.damedashstudios.com";*/
//http://alleyezonme.groundwurk.com/#/
 
 //var URL_LINK = "http://34.197.85.7";

var URL_LINK = "http://www.damedashstudios.com";
var token = "";
  var resumeFlag = "resumeFlag";
  var windowWidth;
  var enableDebugMode = false;
var ServerLamdaUrl = "https://k7vtbcet2h.execute-api.us-east-1.amazonaws.com/latest/"
var appName = "damedashdev"
//modified by J Sachin Singh on 18 August 2016 for declaring variables required for registration.js
/*variables for storing device details and user details locally*/
var storeSliderVisible = 0;
var isEpisodesBtnVisible;
var keyModel="modelValue";
 var keyManufacturer="manufacturerValue";
 var keyDeviceName="device_nameValue";
 var keyDeviceId="device_idValue";
 var keyDeviceMacAddress="device_mac_addressValue";
 var keyBrandName="brand_nameValue";
 var keyHostName="host_nameValue";
 var keyDisplayName="display_nameValue";
 var keySerialNumber="serial_numberValue";
 var keyUUIDValue="uuid_numberValue";
 var codeValue="c_numberValue";
 var _idValue="id_numberValue";
 var pairStatus="pairStatusValue";
 var damedash_uuidExist = "damedash_uuidExist";
 var accessToken = "accessToken"
var STOREVISIBLE = false;
var episodeNumberPush = [];
 var userId="userIdVal";
 var user="userValue";
 var password="passwordValue";
 var email="emailValue";
 var fullName="fullNameValue";
 var created="createdValue";
 var last_login="last_loginValue";
 var logoutTime="logoutTimeValue";
 var subscription_type="subscription_typeValue";
 var subscription_start="subscription_startValue";
 var subscription_end="subscription_endValue";
 var subscription_id="subscription_idValue";
 var subscription_plan_id="id_numberValue";
 var subscription_status="subscription_statusValue";
 var user_status="user_statusValue";
 var user_from="user_fromValue";
 var user_id="user_idValue";
 var customer_id="customer_idValue";
 var imageUserId="imageValue";
 var damedash_id="damedash_idValue";

 /*end of storing device details and user details locally*/

 /* variables in landing page */

 var episode_num=1;
 var season_num = 0;  
 var flagRecentTest=0;
 var flagEpisodeVal=0;
 var intervalCount=0;
 var seekTimeReturned;
 var divNumber;
 var flagEpisodeVisible=0;
 var slideBackClickCount=0;
 var seekTimeValue;
 var flagValuePause=0;
 var StoreValue;
 var flagRecent=0;
 var flagSeekTimeValue=0;
 
var storingIn;
var launchCheckUUIDExist = 0;
var registrationFlagTrue = 0;

 var flagVideoDiv=0;
  var globalassetId;
 var userratingsum;
 var avgAssetRating;
 var startRateCount = 0;
 var myListIdValueInStr;
 var flagListValue=0;
 var videoFileId;
 var myListIdValue;
 var flagListBackValue=0;         
 var tvShowsEpisodes;
 var count=0;
 var flagMyList=0;
 var countEpisodeVideoClick=0;
 var flagRecent=0;
 var flagAdd=0;
 var flagAddRem=0;
 var isDescriptionDivVisible;
 var isThumbsListVisible;
 var episodeClick=1;
 var flagVariable=0;
 var flagEnterVariable=0;
 var flagDivVal=0;
 var flagRemVal=0;
 var detailsTest;
 var myListTest=[];
 var flagPresentInList=0;
 var flagAssetStatus=0;
 var descriptionDisplayValue;
 var carouselNameValue=[];
 var carouselOrder=[];
 var countDownClickVar=0;
 var countDownClickVarLanding=0;
 var carouselCountId=0;
 var loadAllDivsIsTrue=0;
  var div1IsEmpty=0;
  var div2IsEmpty=0;
  var div3IsEmpty=0;
  var div4IsEmpty=0;
  var divList=0;
  var divRecent=0;
 var addRemoveListCount=0;
 var episodeResumeClick=1;
 var watchedVideoFlag=0;
 var myListPresentFlag;
 var flagListAddRemove=0;
 var descriptionDivFlag=0; 
 var flagDescribeVisible=0;
 // modified by Siddartha on 18 Aug 1451pm  
 /* variables in Categories page */
 var result = 0;
 var newMargn;
 var hover_inc = 1;
 var right_clicked = 0;
 var images_length;
 var anim = 120;
 var j_parse;
 var j_result;
 var MOV_CAT_NAME;
 var StoreValue;
 //var storingIn;
 var sliderBackClickCount = 0;
 var isVisible;
 var isVisibleMenuDiv;
 var isVisibleMovieDiv;
 var countRightClick = 0;
 var countDownClick = 0;
 var c = 1; // this is for categories change variable.
 var anim_buffer = 120; // this is to add top margin when down arrow is clicked.
 var down_click = 1;
 var episodeClick = 1;
 var f = 2; // written by siddartha variable to check count for sub-titles page. on 29 june 1147am
 var addedtolist = 1;
 var enterClicked = 0;
 var myListData_fetchids = [];
 var count_for_titles = 0;
 var present_status = "p";
 var not_present_status = "np";
 var num_val = 0;
 var left_animated = "false";
 var videoplayer_visibilty_status = "false";
 var Episode_count = 0;
 var Episode_video_played = 0;
 var myListDataLen = 0;
 var temp_time = 0;
 var updatedTime = 0;
 var myVar;
 var myVarTimeUpdate;
 //var countEpisodeVideoClick = 1;
 var no_function = 0; // writting a var for checking whether api call is under process and restrict other functions.. 
 //  variables for duration ....
 var tim_mill;
 var seconds;
 var minutes;
 var hours;
 var taskCompleted=0; // for checking the for loop for tv-show episodes.
 var episodespage_entered="false";
 var thumbIncrement=0;
 var thumbIncrementLeftCat=0;
 var testValue=1;
 /*  End of variables in categories page.*/
var NavItems = [];
var imgSlideData = [];
var CarouselItems = [];
var cnt = 0;
var mylistname = 'My List';
var recentlywatchedname = 'Recently Watched';

var myListAvailable = 0;


var recentlength=0;
var mylistlength=0;
var storelength = 0;
var hideclass;
var categorycount=0;
var categoryValueCount=0;
var rightMov=0;
var topMov=0;
//var topMovSearch = 18.5;
var topMovSearch = 0;
var episodetopMov =0;
var categoryDownClick=0;
var getCategoryValue;
var catLstId;
var searchLstId;
var descriptionFlag=0;
var marginValue;


 var marginValueSearch;
 var rightMovSearch = 0;
var flagElse=0;
var downMov=0;
var upClickCat=0;
var downCount=1;
var flagMainDivVisible=0;
var flagAddList=0;
var countForCarousel=0;
var flagAddListBack=0;
var pageVisible=0;
var landingCarouselFlag=0;
var episodeCarouselFlag=0;
var videoPlayerInEpisodes=0;
var flagRightClickCat=0;
var resumeEpisodeStat=0;
var divVisibleLanding=0;
var rightAssetFound=0;
var flagButtonMovement=0;
var flagResumePlay = 0;
var topmovdownclick;
var topMovSearchDim ;
var topmovdownclickEpisode = 150;
var register;
var userSubscription;
var createData;
var registeredUser;
var flagSeason = 0;
var seasons = [];
var categoryVisibleChange = true;
var flagSeasonPresent = 0;
var episodeDiv = 0;
var episodeAssetDownClick = 1;
var playVidInEpisodes = 0;
var catSelectCount = 1;
var localBackButtonFlag="localBack";
var landingBack ="landBack";
var loadFlag = 0;
 var backLanding = 0;
 var assetNameValue;
 var assetDescriptionValue;
 var assetDurationValue;
 var assetYearValue;
 var assetImageValue;
 var tvShowStatus;
 var assetDataObj;
 var videoUrlAsset;
 var containsObj;
 var seasonNumValue;
 var episodeNumValue;
 var videoStartTime = 0;
 var myCurrentTime;
 var carouselArray =[];
 var keyPadCount = 1;
var character = "";
var countDownKeyPad = 0;
var keyPadValue = 2;
var searchCount = 0;
var searchCountAsset = 0;
var searchRightClick = 0;
var leftClickSearchFun = 3;
 var searchCountValue = 0;
 var flagSearchDescribe = 0;
 var assetDataValues;
 var settingsClick = 0;
 var storage;
 var uniqueIdValue;
 
  var registrationFlagTrue = 0;
 var launchCheckUUIDExistFlag = 0;
var slideShowVal = 0;
var slickVal = 0;
var changeSlick = 0;
var flagAddListRecent = 0;
var flagAppPause = 0;
var catListCount = 0;
var catLastUpdatedVal = 0;
var endVideoFlag = 0;
var nextAssetMyList;
var assetNameImage;
var asetNextName;
var assetNextDesc;
var assetNextId;

var assetMainImageNext;
var myVarNext;
var varCountNext = 15;
var assetNextFlag = 0;
var nxtVidFunc = 0;
var divsNxtFunc;
var videoRedirectActive =1;
var assetNextMyList;
var nextPlayCall = 0;
var carouselValue = 0;
var photoCarouselLen=0;
var slideShower = 0;
var mainDivFlag = 0;
var likesCount = 0;
var flagLike=0;
var videoAssetId;
var imgSlideDataLike = [];
var assetSlideId;
var assetLike;
//var mainUrl = "http://54.158.161.135/";
//var mainUrl = "http://devstore.damedashstudios.com/";

 var timer;
 var timerCategory;
 var bgimage;
 var bgimageCat;
 var storelength = 0;
 var imgSlideData=[];
 var catMenuVal = 0;
var mainDivFlag = 0;
var photoCarouselLen = 0;
var carouselValue = 0;
var slideShower = 0;
var carouselNameData;


