/*
     Module Name :
     File Name   :      enterFunction.js
     Project     :      dev.damedashstudios.com
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      0.0.12 // 
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      functionality for video playing and updating the seek time for every 3 seconds
     Organisation:      Peafowl inc.
*/
function updateSeekTimeAndPlay(assetId) {

var storage = window.localStorage;
    $.ajax({
        type: "POST",
         url: ServerLamdaUrl + "getassetdata?assetId="+assetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(dataAsset) {
          var data = dataAsset.result;
            var videoPlayTime = 0;
            var totalVideoDuration;
            var updateRecentlyWatched = 30;
           // if (watchedVideoFlag == 0) {
                if (playVidInEpisodes == 0) {
                    //if (data[0].subscription_status == "active") {
                        createRecentlyWatchedList(assetDataObj, assetId);
                    //}
                }
          //  }
            hideForVideoPlay(videoPlayerInEpisodes);
            if (data.m3u8_url != null && data.m3u8_url != "") {
               videoUrlAsset = data.m3u8_url;
            } else {
                videoUrlAsset = data.mp4_url;

            }
  
                            var options = {
                    seekTime: 0,
                    successCallback: function(obj) {

                        backHandler();
                        console.log(obj.pos);
                        if (obj.pos) {
                            // Update seek time
                          //  updateSeekTime(id, obj.pos);
                         // updateSeekTime(storingIn,assetId,obj.pos)

                        }
                    },
                    progressCallback: function(obj) {
                     
                        if (obj.action == 'seekTime') {
                            // Update seek time
                            // updateSeekTime(id, obj.pos);
                            updateSeekTime(storingIn,assetId,obj.pos)
                        }

                    },
                    errorCallback: function(errMsg) {
                        console.log("Error! " + errMsg);
                    },
                    orientation: 'landscape'
                }; 
                
                 window.plugins.streamingMedia.playVideo(videoUrlAsset, options);

        },
        error: function(jqXHR, textStatus, errorThrown) {
            // alert('error');
        },
        complete: function() {


        }
    });

}

function updateSeekTime(storingIn,assetId,temp_time)
{
      var storage = window.localStorage;
         var a = temp_time.split(':'); // split it at the colons
    // minutes are worth 60 seconds. Hours are worth 60 minutes.
    var Vseconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
    if (Vseconds > 0) {
                            $.ajax({
                            type: "POST",
                           url: ServerLamdaUrl + "managewatchlist?videoId="+assetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName+ "&seekTime=" + temp_time,
                            data: "",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(data) {

                            },
                            failure: function(errMsg) {
                                //  alert(errMsg);
                            }
                        });

                        }
}
function episodeVideoPlay(assetId, videoUrl) {
    var totalEpiVideoDuration;
  var storage = window.localStorage;
    var videoPlayTime = 0;
    $.ajax({
        type: "POST",
        url: ServerLamdaUrl + "getassetdata?assetId="+assetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(dataResult) {
             var data = dataResult.result;
            hideForVideoPlay(videoPlayerInEpisodes);
               if (data.m3u8_url != null && data.m3u8_url != "") {
               videoUrlAsset = data.m3u8_url;
            } else {
                videoUrlAsset = data.mp4_url;

            }

                  var options = {
                    seekTime: 0,
                    successCallback: function(obj) {
                        backHandler();
                        console.log(obj.pos);
                        if (obj.pos) {
                            // Update seek time
                          //  updateSeekTime(id, obj.pos);
                          updateSeekTime(storingIn,assetId,obj.pos)

                        }
                    },
                    progressCallback: function(obj) {

                        if (obj.action == 'seekTime') {
                            // Update seek time
                            // updateSeekTime(id, obj.pos);
                            updateSeekTime(storingIn,assetId,obj.pos)
                        }

                    },
                    errorCallback: function(errMsg) {
                        console.log("Error! " + errMsg);
                    },
                    orientation: 'landscape'
                }; 
                 window.plugins.streamingMedia.playVideo(videoUrlAsset, options);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // alert('error');
        },
        complete: function() {


        }
    });

}


function resumeButtonVisible(assetId) {
      var storage = window.localStorage;
    var videoResumeUrl;
    videoStartTime = 1;

    $.ajax({
        type: "POST",
        url: ServerLamdaUrl + "getassetdata?assetId="+assetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(dataResume) {
             var data = dataResume.result;
            createRecentlyWatchedList(assetDataObj, assetId);
            if (data.watchedVideo > 0) {
                  if (data.m3u8_url != null && data.m3u8_url != "") {
               videoResumeUrl = data.m3u8_url;
            } else {
                videoResumeUrl = data.mp4_url;

            }
               // alert('resume time '+data[0].watchedVideo);
                var options = {
                    seekTime: data.watchedVideo*1000,
                    successCallback: function(obj) {
                        backHandler();
                        console.log(obj.pos);
                        if (obj.pos) {
                            // Update seek time
                          //  updateSeekTime(id, obj.pos);
                          updateSeekTime(storingIn,assetId,obj.pos)

                        }
                    },
                    progressCallback: function(obj) {

                        if (obj.action == 'seekTime') {
                            // Update seek time
                            // updateSeekTime(id, obj.pos);
                            updateSeekTime(storingIn,assetId,obj.pos)
                        }

                    },
                    errorCallback: function(errMsg) {
                        console.log("Error! " + errMsg);
                    },
                    orientation: 'landscape'
                }; 
                 window.plugins.streamingMedia.playVideo(videoResumeUrl, options);
               // playVideoResume(videoResumeUrl, data[0].id, data[0].watchedVideo, data[0].subscription_status);
            }

        },
        error: function(jqXHR, textStatus, errorThrown) {},
        complete: function() {


        }
    });
}

/*Function for network connectivity checking*/
function checkNetworkConnectivity(){

     var networkState = navigator.connection.type;
         if (networkState == Connection.NONE){
            
              return 0;
        }
        else{
            //var popup = document.getElementById('myPopup');
            //popup.classList.toggle('show');
            return 1;
        }    
}


