 /*
      Module Name :
      File Name   :      addRemoveToMyList.js
      Project     :      dev.damedashstudios.com
      Copyright (c)      Damedash Studios.
      author      :      Sachin Singh J.
      author      :
      license     :
      version     :      1.0.0 // written by sachin for present.
      Created on  :      8 august 12:26 pm
      Last modified on:  30th November
      Description :      Adding or removing from my list functionality.
      Organisation:      Peafowl inc.
 */
 function addOrRemoveFromMyList(assetId) {

    var storage = window.localStorage; 
     $.ajax({
         type: "POST",
        // url: URL_LINK + "/getAssetData",
        url: ServerLamdaUrl + "getassetdata?assetId="+assetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
         data: "",
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         success: function(dataAddRes) {
           
            var data=dataAddRes.result;
             addOrRemoveAssetToList(data.assetId, data, data.userData[0].myList);
         },
         error: function(jqXHR, textStatus, errorThrown) {},
         complete: function() {


         }
     });

 }

 function addOrRemoveAssetToList(id, completeObject, listStatus) {
    var storage = window.localStorage; 
     flagAddList = 1;
     if (listStatus == false || listStatus == true) {

         if (listStatus == false) {
             document.getElementById("episode_action2").textContent = "Adding....";
         } else {
             document.getElementById("episode_action2").textContent = "Removing....";
         }
         var myKeyVals = {
             "manageMyList": {
                 "videoId": id,
                 "userId": storingIn,
                 "data": completeObject
             }
         }
         $.ajax({
             type: "POST",
             url: ServerLamdaUrl + "managemylist?token=" + storage.getItem(accessToken) + "&appname=" + appName+"&videoId="+id,
             // The key needs to match your method's input parameter (case-sensitive).
             data: "",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(data) {
               
                 if (listStatus == false) {
                     flagListAddRemove = 0;
                     flagButtonMovement = 0;
                     $("#episode_action2").removeClass("addlist_active");
                     $("#episode_action2").removeClass("addlist");
                     $("#episode_action2").css({
                         color: 'white'
                     });
                     $("#episode_action2").addClass("addlist1");
                     $("#episode_action2").addClass("addlist_active1");
                     $("#episode_action2").css({
                         color: 'black'
                     });
                     $('#episode_action2').html("Remove from My List");
                     flagEnterVariable = 1;
                     flagRemVal = 0;
                     flagAdd = 1;
                     flagListBackValue = 0;
                     flagAssetStatus = 1;
                 } else if (listStatus == true) {
                     flagButtonMovement = 0;
                     $("#episode_action2").removeClass("addlist_active1");
                     $("#episode_action2").removeClass("addlist1");
                     $("#episode_action2").css({
                         color: 'white'
                     });
                     $("#episode_action2").addClass("addlist");
                     $("#episode_action2").addClass("addlist_active");
                     $("#episode_action2").css({
                         color: 'black'
                     });
                     $('#episode_action2').html("Add to My List");
                     flagEnterVariable = 0;
                     flagRemVal = 1;
                     flagAdd = 1;
                     flagAssetStatus = 0;
                 }
             },
             failure: function(errMsg) {}
         });

     }
 }