/*duration of movie or tv shows*/
function durationOfAsset(duration) {
    var ms = parseInt(duration);
    var seconds = (ms / 1000);
    var minutes = parseInt(seconds / 60, 10);
    seconds = seconds % 60;
    var hours = parseInt(minutes / 60, 10);
    minutes = minutes % 60;
    if (hours == 0) {
        $('#timeSpan').html(minutes + "m");

    } else {
        $('#timeSpan').html(hours + "h " + minutes + "m");

    }
}

/*function for getting year of release*/
function yearOfRelease(yearValue) {

    var tempArray = [];
    tempArray = yearValue.split("-");
    return tempArray[0];

}