/*
     Module Name :
     File Name   :      backButtonFunctionInLanding.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      function for handling back button events upon checking particular div visibility.
     Organisation:      Peafowl inc.
*/
var descriptionDivFlag = 0;
/** Customizing the Amazon Remote Back button behavior  */
window.addEventListener("load", function() {
    window.addEventListener("popstate", function() {

        if (window.history.state !== "backhandler") {
            window.history.pushState("backhandler", null, null);
            backHandler();
        }
    });
    window.history.pushState("backhandler", null, null);
});

function backHandler()
{
              var descriptionDivStatus = $('#descriptionDivDescribe').is(':visible');
            var videoPlayerInAssetDescriptionVisible = $('#video_player').is(':visible');
            var isVideoPlayerInEpisodesVisible = $('#video_player1').is(':visible');
            var episodeDispVisible = $('#episodeMainDiv').is(':visible');
            var ismenuDiv = $('#menuDiv').is(':visible');
            var catVisible = $('#Catecomplete_' + countDownClickVar).is(':visible');
            var isMainDiv = $('#MainDiv').is(':visible');
            var catSelectVisible = $('#categorySelectMain').is(':visible');
            var searchDivVisible = $('#divSearch').is(':visible');
            var settingsDisplay = $('#divSettingsMain').is(':visible');

            if (descriptionDivStatus) {
                //alert('in descDiv');
                // window.location.reload();
                var storage = window.localStorage;
                episodeClick = 1;
                descriptionFlag = 1;
                if (flagMainDivVisible == 0) {
                    if (flagSearchDescribe == 1) {
                        flagSearchDescribe = 0;
                        $("#descriptionDiv").fadeOut();
                        $("#landingPageSearch").fadeOut();
                        $("#divSearch").fadeIn();
                        //assetDetailsSplit(assetDataValues);
                      
                    } else {
                        $("#descriptionDivDescribe").fadeOut();
                        $('#divIDImg').fadeOut();
                        $("#title").fadeOut();
                        $("#timeSpan").fadeOut();
                        $("#movieReleaseDisp").fadeOut();
                        $("#movieDescribe").fadeOut();
                        $("#buttonShow").fadeOut();
                        $("#landingPage").fadeIn();
                        $("#descTxt").fadeIn();
                        $("#desc_cont_landingPage").fadeIn();
                        $("#movieThumbnails").fadeIn();
                        $("#divID").fadeIn();
                        if (flagAddList == 1) {
                            flagAddListBack = 1; // flag for defining als slider
                            $("#thumbsList").html('');
                            countDownClickVar = 0;
                            cnt = 0;
                            loadFlag = 1;
                            backLanding = 1;
                            storage.setItem(landingBack, backLanding);
                            storage.setItem(localBackButtonFlag, loadFlag);
                            window.location.href = 'index.html';
                            /* getMyList();
                             recentwatched();
                             getCarouselNames();*/
                            index.reload();
                            flagAddList = 0;
                        }
                    }
                } else if (flagMainDivVisible == 1) {

                    $("#descriptionDivDescribe").fadeOut();
                    $('#divIDImg').fadeOut();
                    $("#title").fadeOut();
                    $("#timeSpan").fadeOut();
                    $("#movieReleaseDisp").fadeOut();
                    $("#movieDescribe").fadeOut();
                    $("#buttonShow").fadeOut();
                    $("#catBackground").fadeIn();
                    $("#descTxtCat").fadeIn();
                    $("#Catecomplete_" + countDownClickVar).fadeIn();
                    $("#image1").fadeIn();
                    backLanding = 1;
                    storage.setItem(landingBack, backLanding);
                }


            } else if (videoPlayerInAssetDescriptionVisible) {
                //alert('in videoplayer');
                //alert('test');
                flagVideoDiv = 1;
                if (videoPlayerInEpisodes == 0) {

                    $("#video_player").html('');
                    $("#video_player").hide();
                    $("#videoLoad").hide();
                    $("#descriptionDivDescribe").show();
                    if (flagMainDivVisible == 0) {
                        if (flagSearchDescribe == 1) {
                            $("#landingPageSearch").show();
                        } else {
                            $("#landingPage").show();
                        }
                    } else {
                        $("#catBackground").show();
                    }

                    $("#buttonShow").show();
                    $(".ddlogobottom").show();
                    //$(".blackscreen_landingPage").show();
                    $("#movieDescribe").show();
                    $("#title").show();
                    $("#divID").show();
                    clearInterval(myVar);
                    if (flagResumePlay == 1) {
                        flagResumePlay = 0;
                        clearInterval(myVarTimeUpdate);
                    }
                } else {

                    $("#video_player").html('');
                    $("#video_player").fadeOut();
                    $("#videoLoad").hide();
                    $("#episodeMainDiv").fadeIn();
                    videoPlayerInEpisodes = 0;
                    clearInterval(myVar);
                    if (flagResumePlay == 1) {
                        flagResumePlay = 0;
                        clearInterval(myVarTimeUpdate);
                    }
                }

                //$("#sub_heading").show(); 
            } else if (isVideoPlayerInEpisodesVisible) {
                //alert('in videoplayer1');

            } else if (episodeDispVisible) {
                /// alert('in episodeDispVisible');
                flagEpisodeVisible = 1;
                playVidInEpisodes = 0;
                slideBackClickCount++;
                episode_num = 1;
                season_num = 1;
                episodetopMov = 0;
                seasons = [];
                episodeNumberPush = [];
                episodeAssetDownClick = 1;
                episodeDiv = 0;
                landingCarouselFlag = 1;
                episodeCarouselFlag = 0;
                $("#episodeList").removeClass("episodeInactive");
                $("#episodeDescription").removeClass("episodeInactive");
                $('#episodeTitle').empty();
                $('#episodeButtons').empty();
                $("#episodeMainDiv").fadeOut();
                if (flagMainDivVisible == 0) {
                    if (flagSearchDescribe == 1) {
                        $("#landingPageSearch").show();
                    } else {
                        $("#landingPage").show();
                    }
                } else if (flagMainDivVisible == 1) {
                    $("#catBackground").fadeIn();
                }
                $("#descriptionDiv").fadeIn();
                $("#descriptionDiv").fadeIn();
                $("#descriptionDivDescribe").fadeIn();
                $("#title").fadeIn();
                $("#timeSpan").fadeIn();
                $("#movieReleaseDisp").fadeIn();
                $("#movieDescribe").fadeIn();
                $("#buttonShow").fadeIn();
            } else if (catVisible == true && ismenuDiv == false) {

                flagListBackValue = 0;

                $("#landingPage").fadeOut();
                $("#CategoryPage").fadeIn();
                $("#menuDiv").fadeIn();
                categoryDownClick = 0;
                categoryValueCount = 0;
                rightMov = 0;
                topMov = 0;
                var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                assetDetailsSplitCat(assetData);
                //$("#Catecomplete_"+countDownClickVar).fadeIn();
                $("#image1").hide();
                $("#image1").css({
                    marginLeft: "0px"
                });
                $("#descTxtCat").fadeOut();
                $("#Catecomplete_" + countDownClickVar).animate({
                    marginTop: "0px"
                }, 500);
                $("#CategroyDataDiv").css("width", "");
                $("#CategroyDataDiv").css("marginRight", "");
                $(".categoryData .thumbsList li").css("width", "");
            } else if (isMainDiv && mainDivFlag == 1) {
                flagListBackValue = 0;
                divVisibleLanding = 0;
                pageVisible = 1;
                $("#MainDiv").fadeOut();
                countDownClickVar = 0;
                categoryValueCount = 0;
                $("#categorySelectMain").fadeIn();
                $("#" + catSelectCount).addClass("categoriesDiv_active");
                 mainDivFlag = 0;
            } else if (catVisible == true && ismenuDiv == true) {
                var storage = window.localStorage;

                pageVisible = 0;
                divVisibleLanding = 1;
                 mainDivFlag = 1;
                $("#MainDiv").fadeIn();
                $("#landingPage").fadeIn();
                $("#thumbsList").fadeIn();
                $(".thumbsCont_landingPage").fadeIn();
                $("#catBackground").fadeOut();
                $("#CategoryPage").fadeOut();
                $("#menuDiv").fadeOut();
                $("#Catedata_" + countDownClickVar).removeClass("cate_buttonactive");
                $("#Catecomplete_" + countDownClickVar).fadeOut();
                countDownClickVar = 0;
                categoryValueCount = 0;
                if (flagAddList == 1) {

                    flagAddListBack = 1; // flag for defining als slider
                    $("#thumbsList").html('');
                    countDownClickVar = 0;
                    cnt = 0;
                    loadFlag = 1;
                    backLanding = 1;
                    storage.setItem(landingBack, backLanding);
                    storage.setItem(localBackButtonFlag, loadFlag);

                    window.location.href = 'index.html';
                    /* getMyList();
                     recentwatched();
                     getCarouselNames();*/
                    flagAddList = 0;
                }
            } else if (catSelectVisible) {
                var storage = window.localStorage;
                 if (flagAddList == 1) {
                            flagAddListBack = 1; // flag for defining als slider
                            $("#thumbsList").html('');
                            countDownClickVar = 0;
                            cnt = 0;
                            loadFlag = 1;
                            backLanding = 1;
                            storage.setItem(landingBack, backLanding);
                            storage.setItem(localBackButtonFlag, loadFlag);
                            window.location.href = 'index.html';
                            index.reload();
                            flagAddList = 0;
                        }
                        else{
                             $("#categorySelectMain").fadeOut();
                             $("#MainDiv").fadeIn();
                             $("#landingPage").fadeIn();
                             mainDivFlag = 1;
                        }
            } else if (searchDivVisible) {
               /* mainDivFlag = 1;
                $("#divSearch").hide();
                $("#MainDiv").show();
                $("#landingPage").show();*/

                 if(flagAddList == 1){
                    window.location.href = 'index.html';
                }
                else{
                     mainDivFlag = 1;
                $("#divSearch").hide();
                $("#MainDiv").show();
                $("#landingPage").show();
                }
            } else if (settingsDisplay) {
                mainDivFlag = 1;
                $("#divSettingsMain").hide();
                $("#categorySelectMain").fadeIn();
            }
            else if(slideShower == 1){
                mainDivFlag = 1;
               slideShower = 0;
                $("#MainDiv").css({ opacity: 1 });
                $("#descTxt").fadeIn();
                $("#landingPage").fadeIn();
                $("#desc_cont_landingPage").fadeIn();
                $('#movieThumbnails').css({ opacity: 1 });
                $("#divID").fadeIn();
                $("#landingPage").fadeIn();
                $('#slideShowImg').css({ opacity: 0 });
            }  
}