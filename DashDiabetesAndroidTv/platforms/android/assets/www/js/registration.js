 /**
  *
   Module Name :      
   File Name   :      registration.js
   Project     :      damedashstudios
   Copyright (c)      Damedash Studios.
   author      :      J Sachin Singh.
   author      :      
   license     :   
   version     :      0.0.12  
   Created on  :      17 July 12:20 pm
   Last modified on:  30th November 
   Description :      This file contains javascript functionality for pairing of device i.e for getting activation code 
                      and storing the user details in local storage.  
   Organisation:      Peafowl inc.
  */
 function createDataBase() {
   
     var storage = window.localStorage;
     storage.setItem(keyModel, device.model);
     storage.setItem(keyManufacturer, device.manufacturer);
     storage.setItem(keyDeviceId, device.uuid);
     storage.setItem(keyDeviceMacAddress, "");
     storage.setItem(keyBrandName, "android");
     storage.setItem(keyHostName, "android");
     storage.setItem(keyDisplayName, "android");
     storage.setItem(keySerialNumber, device.serial);
     var value = storage.getItem(keyDisplayName);
     var myKeyVals = {
         "getActivationCode": {
             "model": storage.getItem(keyModel),
             "manufacturer": storage.getItem(keyManufacturer),
             "device_name": storage.getItem(keyManufacturer),
             "device_id": storage.getItem(keyDeviceId),
             "device_mac_address": storage.getItem(keyDeviceId),
             "brand_name": storage.getItem(keyManufacturer),
             "host_name": "ip-10-11-3-68",
             "display_name": storage.getItem(keyManufacturer),
             "serial_number": storage.getItem(keySerialNumber)
         }
     }
     $.ajax({
         type: "POST",
        // url: URL_LINK + "/getActivationCode",
       url: ServerLamdaUrl+"getactivationcode?appname="+appName+"&model="+storage.getItem(keyModel)+"&manufacturer="+storage.getItem(keyManufacturer)+"&device_name="+storage.getItem(keyManufacturer)+"&device_id="+storage.getItem(keyDeviceId)+"&device_mac_address="+storage.getItem(keyDeviceId)+"&brand_name="+storage.getItem(keyManufacturer)+"&host_name=app&display_name="+storage.getItem(keyManufacturer)+"&serial_number="+storage.getItem(keySerialNumber)+"&version="+appName,
         // The key needs to match your method's input parameter (case-sensitive).
         data: "",
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         success: function(data) {
         
          
             storage.setItem(codeValue, data.result.code);
             storage.setItem(keyUUIDValue, data.result.uuid);
             storage.setItem(_idValue, data.result._id);
             storage.setItem(pairStatus, data.result.pair_devce);

         },
          error: function(jqXHR, textStatus, errorThrown) {
                        
          },
          complete: function() {
           
             
           // if ( storage.getItem(pairStatus) == "active") {
            getAccessToken();
           // window.location.href = 'registration.html';
          }
         
     });
 }

 function getRegisteredData(isInRegistrationHTML) {
   
     var storage = window.localStorage;
     // alert('uuid '+storage.getItem(keyUUIDValue))
     var myKeyVals = {
         "getAccountInfo": {
             "deviceId": storage.getItem(keyDeviceId),
             "uuid": storage.getItem(keyUUIDValue)
         }
     }
     $.ajax({
         type: "POST",

        // url: URL_LINK + "/getAccountInfo",

         url: ServerLamdaUrl + "getaccountinfo?token=" + storage.getItem(accessToken) + "&appname=" + appName,
         // The key needs to match your method's input parameter (case-sensitive).
         data: "",
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         success: function(dataReg) {
           if(dataReg.statusCode == '200'){
              var data = dataReg.result;
             //alert('Activation code : '+data.code);

             storage.setItem(userId, data._id);
             storage.setItem(user, data.user);

             storage.setItem(password, data.password);
             storage.setItem(email, data.email_id);

             storage.setItem(fullName, data.full_name);
             storage.setItem(created, data.created);
             storage.setItem(last_login, data.last_login);

             storage.setItem(logoutTime, data.logoutTime);
             storage.setItem(subscription_type, data.subscription_type);
             storage.setItem(subscription_start, data.subscription_start);
             if(data.webAppLogin == "Auth0withNoSubscription"){
                storage.setItem(subscription_end, "");
             }
             else{
                storage.setItem(subscription_end, data.subscription_end);
             }
             
             storage.setItem(subscription_id, data.subscription_id);
             storage.setItem(subscription_plan_id, data.subscription_plan_id);

             storage.setItem(subscription_status, data.subscription_status);
             storage.setItem(user_status, data.user_status);
             storage.setItem(user_from, data.user_from);

             storage.setItem(user_id, data.user_id);
             storage.setItem(customer_id, data.customer_id);
             storage.setItem(imageUserId, data.image);

             storage.setItem(damedash_id, data.damedash_id);
             storage.setItem(damedash_uuidExist, data.uuid_exist);
           }
            else{
               storage.setItem(damedash_uuidExist, "false");
            }
           
             

         },
         error: function(jqXHR, textStatus, errorThrown) {
            
          },
          complete: function() {
               if ( storage.getItem(damedash_uuidExist) == "true") {
                // registrationFlagTrue = 0;
                if(isInRegistrationHTML== true && storage.getItem("user_statusValue") == 'active')
                {
                 
                 window.location.href = 'index.html';
             }
             else if(isInRegistrationHTML== false){
               
                        storingIn=storage.getItem('userIdVal');
                       // var storage = window.localStorage;
                storage.setItem(resumeFlag, "true");
                 mainDivFlag = 1;
               
                        $("#registrationMain").fadeOut("slow");
                            $("#MainDiv").show();
                            $('#landingPage').show();
                            $(".preloaderContDescribe").fadeIn();
                            getMyList();
                           // getAccessToken();
             }
             }
             else { 
            if(isInRegistrationHTML == true){
                 storingIn=storage.getItem('userIdVal');
                       // var storage = window.localStorage;
                storage.setItem(resumeFlag, "true");
                 mainDivFlag = 1;
               
                        $("#registrationMain").fadeOut("slow");
                            $("#MainDiv").show();
                            $('#landingPage').show();
                            $(".preloaderContDescribe").fadeIn();
                            getMyList();
            }
                else {

                            storage.removeItem(codeValue);
                           storage.removeItem(keyUUIDValue);
                           storage.removeItem(_idValue);
                           storage.removeItem(pairStatus);
                           storage.removeItem(userId);
                             storage.removeItem(user);

                             storage.removeItem(password);
                             storage.removeItem(email);

                             storage.removeItem(fullName);
                             storage.removeItem(created);
                             storage.removeItem(last_login);

                             storage.removeItem(logoutTime);
                             storage.removeItem(subscription_type);
                             storage.removeItem(subscription_start);

                             storage.removeItem(subscription_end);
                             storage.removeItem(subscription_id);
                             storage.removeItem(subscription_plan_id);

                             storage.removeItem(subscription_status);
                             storage.removeItem(user_status);
                             storage.removeItem(user_from);

                             storage.removeItem(user_id);
                             storage.removeItem(customer_id);
                             storage.removeItem(imageUserId);

                             storage.removeItem(damedash_id);
                             storage.removeItem(damedash_uuidExist);
                             storage.removeItem(accessToken);
                            createDataBase();
                            launchCheckUUIDExistFlag = 0;
                            //window.location.href = 'registration.html';
                        }

        }       
          }
     });

 }

 function getAccessToken() {
     var storage = window.localStorage;
   
      $.ajax({
         type: "POST",
        // url: URL_LINK + "/getActivationCode",
       url: ServerLamdaUrl+"getaccesstoken?appname="+appName+"&uuid="+storage.getItem(keyUUIDValue)+"&deviceID="+storage.getItem(keyDeviceId),
         // The key needs to match your method's input parameter (case-sensitive).
         data: "",
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         success: function(data) {
         
           if(data.statusCode!= 401){
             storage.setItem(accessToken, data.result.token);
            getRegisteredData(true);
           }
           else{
             window.location.href = 'registration.html';
           }
           
         },
          error: function(jqXHR, textStatus, errorThrown) {
            
          },
          complete: function() {
            // alert(storage.getItem(codeValue));
            //getMyList();
           /* if ( storage.getItem(pairStatus) == "active") {
                 window.location.href = 'index.html';
             }else{
                window.location.href = 'registration.html';
             }*/
             // window.location.href = 'registration.html';
             //getRegisteredData(true);
            
          }
     });
 }